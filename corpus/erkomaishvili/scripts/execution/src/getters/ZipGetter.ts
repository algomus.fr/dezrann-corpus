import AdmZip from "adm-zip";
import Util from "../util/FSUtil";
import { execSync } from "child_process";
import { Directory } from "../util/types";

export default class ZipGetter {
    private zipPath: string;
    private instance: Util;


    constructor(private name: string) {
        this.instance = Util.getInstance();
    }

    getData(source: string, zip: boolean, output: string = this.instance.join(process.env.RESOURCES_PATH, this.name)): Directory {
         if (zip) this.zipPath = `${output}.zip`;
        this.createResourcesDirectory(this.instance.removeLast(output, "/"));

        return this.extractZip(source, zip, output);
    }

    private createResourcesDirectory(output: string) {
        this.instance.createDirsFromPaths(false, output);
    }

    private extractZip(source: string, zip: boolean, output: string): Directory {
        let tmpOutput: string = this.instance.removeLast(output, "/");

        execSync(`wget -N -P ${tmpOutput} ${source}`, { stdio : [null, null, null]} );
        
        if (zip) {

            const zip: AdmZip = new AdmZip(this.zipPath);
            zip.extractAllTo(tmpOutput);

            this.deleteZipAndMACOSX(tmpOutput);

        } else {
            this.instance.createDirsFromPaths(false, tmpOutput);
            this.instance.renameSync(this.instance.join(tmpOutput, source.split("/").pop()), output);
        }
        let stat = this.instance.statSync(output);
        if(stat.isFile()) output = tmpOutput;

        return this.instance.getFiles(output);
    }

    private deleteZipAndMACOSX(output: string) {
        this.instance.removeDirsFromPaths(this.instance.join(output, '__MACOSX'), this.zipPath);
    }
}
