import ZipGetter from "./getters/ZipGetter";
import DataSaver from "./savers/DataSaver";
import FSUtil from "./util/FSUtil";
import { Directory, File } from "./util/types";
import id3 from "node-id3";


export default class AudioCollector {
    private instance: FSUtil;

    private getter: ZipGetter;
    private IDList: string[];
    private audioList: Directory | null;
    private saver: DataSaver;

    constructor(private download: boolean, private exec: boolean) {
        this.instance = FSUtil.getInstance();

        this.getter = new ZipGetter(process.env.AUDIO_NAME);
        this.saver = new DataSaver(process.env.AUDIO_DIR_NAME);

        this.audioList = {
            path: process.env.AUDIO_DIR_NAME,
            valueList: []
        }
        
        this.IDList = JSON.parse(this.instance.readFileSync(this.instance.join(process.env.RESOURCES_PATH, process.env.ID_FILE)).toString());
    }

    // The way to manage data has changed :
    // All data can't be saved in a variable
    // So we need to do piece by piece
    async collectAudio() {
        let index = 0;
        if (!this.download && this.exec) {
            process.stdout.write("Searching audio resources... ");
            if (this.instance.existsSync(this.instance.join(process.env.RESOURCES_PATH, process.env.AUDIO_NAME)) === false) {
                this.audioList = null;
                console.log("ERROR : no such file or directory");
                return;
            }
            console.log("OK");
        }
        for (let id of this.IDList) {
            this.audioList = {
                path: id,
                valueList: []
            }
            if (this.download) this.getAudioTab(id);
            else this.readAudioTab(id);
            
            if (this.exec) this.saveAudioTab(index);

            index++;
        }
    }

    private getAudioTab(id: string) {
        process.stdout.write(`Getting from ${id}... `);

        this.getData(id, "mix");
        this.getData(id, "S1");
        this.getData(id, "S2");
        this.audioList = this.getData(id, "S3");

        console.log("OK");
    }

    private readAudioTab(id: string) {
        process.stdout.write(`Reading from ${id}... `);
        this.audioList = this.instance.getFiles(this.instance.join(process.env.RESOURCES_PATH, process.env.AUDIO_NAME, `${id}`));
        console.log("OK");
    }

    private getData(id: string, id2: string): Directory {
        let source: string = this.instance.join(process.env.AUDIO_SOURCE_PATH, `${id}_Erkomaishvili`, `${id}_Erkomaishvili_${id2}.mp3`);
        let output: string = this.instance.join(process.env.RESOURCES_PATH, process.env.AUDIO_NAME, `${id}`, `${id}_${id2}.mp3`);

        let res: Directory = this.getter.getData(source, false, output);

        return res;
    

    }

    private saveAudioTab(id: number) {
        process.stdout.write("Saving info... ");
        for (let file of this.audioList.valueList) {
            
            let meta = id3.read(file.path);
            let newVal = id3.write(meta, (file as File).value as Buffer);


            (file as File).value = newVal.toString("binary");

            
            this.saver.saveData(file, id, true, true);
        }
        console.log("OK");
    }
}