import Util from "../util/FSUtil";

export default class RepositoryMaker {
    instance: Util;

    constructor() {
        this.instance = Util.getInstance();
    }

    makeRepo(IDList: string[]) {
        this.initRepo();

        this.instance.createDirsFromPaths(true, ...IDList.map(id => this.instance.join(process.env.FINAL_PATH, id)));
    }

    private initRepo() {
        this.instance.createDirsFromPaths(false, process.env.FINAL_PATH);
    }
}