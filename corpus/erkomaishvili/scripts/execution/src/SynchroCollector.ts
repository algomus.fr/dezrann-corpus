import ZipGetter from "./getters/ZipGetter";
import SynchroParser from "./parsers/SynchroParser";
import DataSaver from "./savers/DataSaver";
import FSUtil from "./util/FSUtil";
import { Directory } from "./util/types";

export default class SynchroCollector {
    private getter: ZipGetter;
    private parser: SynchroParser;
    private saver: DataSaver;
    private synchroList: Directory | null;
    private parsedList: Directory;
    private instance: FSUtil;
    
    constructor(private download: boolean, private exec: boolean) {
        this.instance = FSUtil.getInstance();

        this.getter = new ZipGetter(process.env.SYNCHRO_NAME);

        this.parser = new SynchroParser(process.env.SYNCHRO_DIR_NAME, parseInt(process.env.FILES_PER_DIR));

        
        this.saver = new DataSaver(this.instance.removeLast(process.env.SYNCHRO_DIR_NAME, "/"));
    }

    collectSynchro() {

        if (this.download) this.getSynchroZip();
        else this.readSynchro();

        if (this.exec && this.synchroList) {
            this.parseSynchro();

            this.saveSynchro();
        }
    }

    private getSynchroZip() {
        process.stdout.write("Getting synchro source... ");
        this.synchroList = this.getter.getData(process.env.SYNCHRO_SOURCE_PATH, true);
        console.log("OK");
    }

    private readSynchro() {
        process.stdout.write("Reading synchro resources... ");
        if (this.instance.existsSync(this.instance.join(process.env.RESOURCES_PATH, process.env.SYNCHRO_NAME)) === false) {
            this.synchroList = null;
            console.log("ERROR : no such file or directory");
            return;
        }
        this.synchroList = this.instance.getFiles(this.instance.join(process.env.RESOURCES_PATH, process.env.SYNCHRO_NAME));
        console.log("OK");
    }

    private parseSynchro() {
        process.stdout.write("Parsing synchro... ");
        this.parsedList = this.parser.parseData(this.synchroList);
        console.log("OK");
    }

    private saveSynchro() {
        process.stdout.write("Saving synchro... ");
        for (let i = 0; i < this.parsedList.valueList.length; i++)
            this.saver.saveData(this.parsedList, i);
        console.log("OK");
    }
}