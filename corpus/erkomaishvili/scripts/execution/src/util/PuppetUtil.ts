import { Page, ElementHandle, Browser } from 'puppeteer';
import puppeteer from "puppeteer";

type AttributePath = {
    parent: string,
    path: string,
    attribute: string
}

export default class PuppetUtil {
    private static instance: PuppetUtil;
    private browser: Browser;
    private page: Page;

    private constructor() {}

    static getInstance(): PuppetUtil {
        if (!PuppetUtil.instance) PuppetUtil.instance = new PuppetUtil();

        return PuppetUtil.instance;
    }

    async initPage() {
        this.browser = await puppeteer.launch({ handleSIGINT: false });
        this.page = await this.browser.newPage();
    }

    closePage() {
        this.page.close();
        this.browser.close();
    }

    goto = (link: string) => this.page.goto(link, { waitUntil: "networkidle0" });

    async getTagsContent(...attributes: AttributePath[]): Promise<string[][]> {
        let res: string[][] = [];
        for (let attr of attributes) {
            const element: ElementHandle<Element> = await this.page.$(attr.parent);

            res.push(await this.getContentList(this.page, element, attr.path, attr.attribute));
        }

        return res;
    }

    private async getContentList(page: Page, parent: ElementHandle<Element>, ids: string, attribute: string): Promise<string[]> {
        return await page.evaluate((parent, ids, attribute) => {
            return Array.from(parent.querySelectorAll(ids)).map(element => {
                if (attribute == "text") return element.textContent;
                return element.getAttribute(attribute);
            });
        }, parent, ids, attribute);
    }
}