export type File = {
    path: string;
    value: string | Buffer;
}

export type Directory = {
    path: string,
    valueList: (File | Directory)[]
}

// Represents an element in a .json file, with the "onset" and "date" numbers
export type JSONData = {
    onset: number,
    date: number,
}

// Needed when creating mix synchro, for the mean computation
export type DivCount = {
    json: JSONData,
    div: number
}