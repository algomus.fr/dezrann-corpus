# Resources from AudioLabs

These scripts get the [resources given by AudioLabs](https://www.audiolabs-erlangen.de/resources/MIR/2019-GeorgianMusic-Erkomaishvili#rec "Homepage for Erkomaishvili's corpus"), and parse them into the standard corpus structure for [Dezrann](www.dezrann.net/ "Dezrann's homepage").

## How to

### Install dependencies

```bash
yarn install
```

### Build runnable code, get and parse resources

```bash
yarn run corpus
```

Launches the script "index.js" :
    - Downloads zip files and extract them in "resources" folder;
    - Prepare resources to match parsers and final result requirements (In a Directory/File type structure);
    - Parses resources to parse (csv files into synchro.json files).
    - Saves files in "corpus" folder.

There are options :
    - with `--download`, the script only extracts data and places it in "resources" folder;
    - with `--exec`, the script takes data in "resources" folder and parses/saves it in final "corpus" folder;
    - if used together, these have the same effect as no options;
    - with `--filter`, the script will only work on the resources you want (only acceptable words : xml, synchro, audio, info; they can be used together) (do not use it with `--download` option for now).

#### Dev mode

To run directly `index.ts` without building the `index.js`:

```bash
yarn corpus-dev
```


### Clean the scores

Remove the spurious time signatures and lyrics with the quarter note reference.

```sh
python3 clean-scores.py
```
