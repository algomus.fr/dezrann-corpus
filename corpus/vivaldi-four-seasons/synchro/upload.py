
import glob
import os

for f in glob.glob('*.json'):
    key = '-'.join(f.split('-')[1:3])

    cmd = 'scp %s dezrann@alb:corpus/vivaldi/%s/sources/audios/%s/synchro.json' % (f, key, key)
    print(cmd)
    os.system(cmd)