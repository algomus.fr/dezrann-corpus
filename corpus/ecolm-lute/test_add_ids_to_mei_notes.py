import unittest

import lxml.etree as ET

from add_ids_to_mei_notes import add_ids_to_notes, MEI_NAMESPACE, XML_NAMESPACE


class TestAddIDsToMeiNotes(unittest.TestCase):

    def test_add_ids_to_notes_works(self):
        mei_content = '''
            <mei xmlns="http://www.music-encoding.org/ns/mei">
                <meiHead></meiHead>
                <music>
                    <note/>
                    <note xml:id="n3"/>
                    <note/>
                </music>
            </mei>
        '''
        tree = ET.ElementTree(ET.fromstring(mei_content))
        root = tree.getroot()
        notes = root.findall(".//mei:note", MEI_NAMESPACE)
        ids = [note.get(f"{{{XML_NAMESPACE['xml']}}}id") for note in notes]
        self.assertFalse(all(ids))
        add_ids_to_notes(tree)
        ids = [note.get(f"{{{XML_NAMESPACE['xml']}}}id") for note in notes]
        self.assertTrue(all(ids), "Not all notes have an ID")
        self.assertEqual(len(ids), len(set(ids)), "There are duplicate IDs")


if __name__ == '__main__':
    unittest.main()
