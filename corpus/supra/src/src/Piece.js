const sharp = require('sharp');
const fs = require('fs');
const axios = require('axios');

const stream = require('stream');
const { promisify } = require('util');
const finished = promisify(stream.finished);
const midiParser = require('midi-parser-js');
const cheerio = require('cheerio');

class Piece {
  constructor(parsedPiece, options) {
    const split = parsedPiece.sdr.split('/');
    this.id = split[split.length - 1];
    this.label = parsedPiece.label.replace(' ', '_');
    this.data = parsedPiece
    this.pieceDir = `pieces/${this.label}_${this.id}`;
    this.sourcesDir = `${this.pieceDir}/sources`;
    this.analysesDir = `${this.pieceDir}/analyses`;
    this.imgFile = `./${this.sourcesDir}/audios/${this.label}/images/roll_512/roll_512.jpg`;
    this.imgUrl = `https://stacks.stanford.edu/image/iiif/${this.id}/${this.id}_0001/full/512,/0/default.jpg?download=true`;
    this.mp3File = `${this.sourcesDir}/audios/${this.label}/${this.label}.mp3`
    this.mp3Url = `https://ccrma.stanford.edu/~craig/supra-audio/welte-red/mp3-exp/${this.id}.mp3`
    this.marcFile = `${this.sourcesDir}/audios/${this.label}/${this.label}.marcxml`
    this.marcUrl = parsedPiece.marcxml
    this.rawMidFile = `${this.sourcesDir}/audios/${this.label}/raw.mid`


    this.skipImage = options.skipImage;
    this.skipMp3 = options.skipMp3;
  }

  id;

  label;

  data;

  pieceDir;

  sourcesDir;

  analysesDir;

  imgFile;

  imgUrl;

  mp3File;

  mp3Url;

  rawMidFile;

  marcFile;

  marcURl;

  deleting = false;

  skipImage;

  skipMp3;

  lastDate;

  synchro = []

  IMAGE_SCALE = 8

  checkCompleteness() {
    if (
      fs.existsSync(this.pieceDir) &&
      fs.existsSync(this.analysesDir) &&
      fs.existsSync(this.sourcesDir) &&
      fs.existsSync(`${this.sourcesDir}/audios`) &&
      fs.existsSync(`${this.sourcesDir}/audios/${this.label}`) &&
      fs.existsSync(`${this.sourcesDir}/audios/${this.label}/images`) &&
      fs.existsSync(`${this.sourcesDir}/audios/${this.label}/images/roll_512`) &&
      fs.existsSync(`./${this.pieceDir}/info.json`) &&
      fs.existsSync(this.imgFile) &&
      fs.existsSync(this.mp3File) &&
      fs.existsSync(this.rawMidFile) &&
      fs.existsSync(`./${this.sourcesDir}/audios/${this.label}/images/roll_512/positions.json`)
    ) {
      return true;
    }
    return false;
  }

  generateFolderHierarchy() {
    if (!fs.existsSync(this.pieceDir))
      fs.mkdirSync(this.pieceDir);
    if (!fs.existsSync(this.analysesDir))
      fs.mkdirSync(this.analysesDir);
    if (!fs.existsSync(this.sourcesDir))
      fs.mkdirSync(this.sourcesDir);
    if (!fs.existsSync(`${this.sourcesDir}/audios`))
      fs.mkdirSync(`${this.sourcesDir}/audios`);
    if (!fs.existsSync(`${this.sourcesDir}/audios/${this.label}`))
      fs.mkdirSync(`${this.sourcesDir}/audios/${this.label}`);
    if (!fs.existsSync(`${this.sourcesDir}/audios/${this.label}/images`))
      fs.mkdirSync(`${this.sourcesDir}/audios/${this.label}/images`);
    if (!fs.existsSync(`${this.sourcesDir}/audios/${this.label}/images/roll_512`))
      fs.mkdirSync(`${this.sourcesDir}/audios/${this.label}/images/roll_512`);
  }

  getYear(str) {
    const leftSplit = str.split('[');
    const rightSplit = leftSplit[leftSplit.length - 1].split(']');
    return rightSplit[0].replace('between ','').replace(' and ','-');
  }

  getEditor(str) {
    const leftSplit = str.split('[');
    leftSplit.pop();
    const rightSplit = leftSplit.join();
    return rightSplit
    .replaceAll(',', '')
    .replaceAll('[', '')
    .replaceAll(']', '')
    .trim()
  }

  async generateInfo() {
    await this.downloadMarcXml();

    const marc = fs.readFileSync(this.marcFile, {
      encoding: 'utf-8'
    })

    let imprintStr = ''
    const $ = cheerio.load(marc)
    const imprintSelector = $('datafield[tag=264]').children()
    for (const item of imprintSelector) {
      const str = $(item).text();
      imprintStr = `${imprintStr} ${str.trim()}`
    }


    const infoJSON = {
      composer: this.data.composer.split('(')[0],
      id: `${this.label}_${this.id}`,
      'quality:musical-time': 0,
      opus: {
        year: this.getYear(imprintStr.trim()),
        contributors: {
          composer: this.data.composer.split('(')[0].trim(),
          performer: this.data.performer.split('(')[0].trim(),
          editor: this.getEditor(imprintStr.trim()),
        },
        'ref:supra': this.id,
        'ref:wm': `${this.label} ${this.id}`,
        imprint: imprintStr.trim(),
      },
      sources: {
        audios: [
          {
            file: `${this.label}.mp3`,
            images: [
              {
                image: `roll_512.jpg`,
                type: 'wave',
              }
            ],
            'onset-date': `synchro.json`
          }
        ],
        license: "CC-BY-NC-SA-4.0",
      },
    }

    // Additional opus information

    const key = $('datafield[tag=240] subfield[code="r"]').text().replace('.', '');
    if (key) infoJSON.opus['key'] = key;

    const opusNum= $('datafield[tag=240] subfield[code="n"]').text().replace(',', '').replace(';', '');
    if (opusNum) infoJSON.opus['opus'] = opusNum;

    const partName = $('datafield[tag=245] subfield[code="p"]').text().replace(' /', '');
    if (partName) {
      infoJSON.opus['piece:title'] = this.data.title;
      infoJSON.opus['movement:title'] = partName;
    }
    else
    {
      infoJSON.opus['title'] = this.data.title;
    }

    fs.writeFileSync(`./${this.pieceDir}/info.json`, JSON.stringify(infoJSON, null, 2));
  }

  async generateSources() {
    if (!this.skipImage)
      await this.downloadImage();
    if (!this.skipMp3)
      await this.downloadMp3();
    await this.downloadMidi();
    await this.generateSynchro();
  }

  async downloadMarcXml() {
    if (this.deleting) return;
    process.stdout.write('Processing MarcXML...');
    await this.downloadFile(this.marcUrl, this.marcFile);
    console.log('done');
  }

  async downloadImage() {
    if (this.deleting) return
    process.stdout.write('Downloading image...');
    await this.downloadFile(this.imgUrl, this.imgFile + '_tmp.jpg');
    if (this.deleting) return
    await sharp(this.imgFile + '_tmp.jpg')
      .rotate(-90)
      .toFile(this.imgFile, () => {
        fs.rmSync(this.imgFile + '_tmp.jpg')
      })
    console.log('done')
  }

  async downloadMp3() {
    if (this.deleting) return
    process.stdout.write('Downloading mp3...');
    await this.downloadFile(this.mp3Url, this.mp3File);
    console.log('done')
  }

  async downloadMidi() {
    if (this.deleting) return
    process.stdout.write('Downloading midi RAW...');
    await this.downloadFile(this.data.midiRaw, this.rawMidFile);
    if (this.deleting) return

    fs.writeFileSync(`./${this.sourcesDir}/audios/${this.label}/images/roll_512/positions.json`, JSON.stringify(this.parseRawMidiFile(this.rawMidFile), null, 2));

    console.log('done')
  }

  parseRawMidiFile(midiFile) {
    if (this.deleting) return
    const midiData = fs.readFileSync(midiFile);
    const midiArray = midiParser.parse(midiData);

    const positions = []

    const startTimeDivision = 568 //568;
    let firstHoleX = 0
    let lastHoleX;
    let imageHeight = 0;
    let rollHeight = 0;
    let bassMargin = 0;
    let trebleMargin = 0

    midiArray.track[0].event.forEach(({ data, deltaTime }) => {
      if (deltaTime === 0) {
        if (typeof data === 'string' && data.includes('@FIRST_HOLE'))
          firstHoleX = parseInt(data.split('\t')[2].split('px')[0]);
        if (typeof data === 'string' && data.includes('@LAST_HOLE'))
          lastHoleX = parseInt(data.split('\t')[2].split('px')[0]);
        if (typeof data === 'string' && data.includes('@IMAGE_WIDTH'))
          imageHeight = parseInt(data.split('\t')[2].split('px')[0]) / this.IMAGE_SCALE;
        if (typeof data === 'string' && data.includes('@ROLL_WIDTH'))
          rollHeight = parseFloat(data.split('\t')[2].split('px')[0]) / this.IMAGE_SCALE;
        if (typeof data === 'string' && data.includes('@HARD_MARGIN_BASS'))
          bassMargin = parseFloat(data.split('\t')[1].split('px')[0]) / this.IMAGE_SCALE;
        if (typeof data === 'string' && data.includes('@HARD_MARGIN_TREBLE'))
          trebleMargin = parseFloat(data.split('\t')[1].split('px')[0]) / this.IMAGE_SCALE;
      }
    })

    positions.push({
      date: 0,
      x: firstHoleX / this.IMAGE_SCALE
    })
    let tempoIncrements = 1
    let tickTime
    let date = 0

    console.log(midiArray.timeDivision)

    midiArray.track[0].event.forEach(e => {
      if (e.metaType === 81 && e.deltaTime > 0) {
        tickTime = (e.data / startTimeDivision)
        date += 3600 * tickTime
        positions.push({
          date: date / 1000000,
          x: (firstHoleX + (3600 * tempoIncrements)) / this.IMAGE_SCALE
        })
        this.synchro.push({
          date: date / 1000000,
          onset: tempoIncrements,
        })
        tempoIncrements++;
      }
    })

    const remainingPixel = (lastHoleX - (firstHoleX + (tempoIncrements - 1) * 3600))

    positions.push({
      date: (date + remainingPixel * tickTime) / 1000000,
      x: lastHoleX / this.IMAGE_SCALE
    })

    this.lastDate = (date + remainingPixel * tickTime) / 1000000;
    this.synchro.push({
      date: this.lastDate,
      onset: tempoIncrements,
    })

    const trebleTop = ((imageHeight - rollHeight) / 2) + trebleMargin
    const trebleBottom = ((imageHeight - rollHeight) / 2) + trebleMargin + 50
    const bassTop = imageHeight - bassMargin - 50
    const bassBottom = imageHeight - bassMargin

    return {
      "staffs": [
        {
          "top": trebleTop,
          "bottom": trebleBottom
        },
        {
          "top": trebleBottom,
          "bottom": trebleBottom + ((bassTop - trebleBottom) / 2)
        },
        {
          "top": trebleBottom + ((bassTop - trebleBottom) / 2),
          "bottom": imageHeight - bassMargin - 50
        },
        {
          "top": bassTop,
          "bottom": bassBottom
        },
      ],
      "date-x": [
        ...positions
      ]
    };
  }

  async downloadFile(url, outputPath) {
    if (this.deleting) return
    try {
      const writer = fs.createWriteStream(outputPath);
      const { data } = await axios({
        method: 'get',
        url,
        responseType: 'stream'
      });
      data.pipe(writer);
      return finished(writer);
    } catch (error) {
      console.log('ERROR, deleting piece')
      this.deleting = true
      this.deletePiece();
    }
  }

  generateSynchro() {
    if (this.deleting) return
    const synchro = [
      {
        "date": 0,
        "onset": 0
      },
      ...this.synchro
    ]

    fs.writeFileSync(`./${this.sourcesDir}/audios/${this.label}/synchro.json`, JSON.stringify(synchro));
  }

  deletePiece() {
    fs.rmSync(this.pieceDir, { recursive: true, force: true });
  }
}

module.exports = Piece;