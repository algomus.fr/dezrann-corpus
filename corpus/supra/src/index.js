const fs = require('fs');
const yargs = require('yargs');
const Piece = require('./src/Piece');
const Scraper = require('./src/Scraper');

const argv = yargs
  .option('only', {
    description: 'Fetch only the given supra label number list\nexample: --only 10 26 40',
    type: 'array'
  })
  .option('skip', {
    description: 'If the piece is already complete, skips it',
    alias: 's',
    type: 'boolean'
  })
  .option('skip-image', {
    description: 'Skips image download and manipulation',
    type: 'boolean'
  })
  .option('skip-mp3', {
    description: 'Skips MP3 download',
    type: 'boolean'
  })
  .help()
  .alias('help', 'h')
  .argv;

const main = async () => {

  const scraper = new Scraper({
    only: argv.only || []
  });
  await scraper.init();

  if (!fs.existsSync('pieces'))
    fs.mkdirSync('pieces')

  for (let parsed of scraper.parsedPieces) {
    console.log(`--- Scraping ${parsed.label} ---`);

    const piece = new Piece(parsed, {
      skipImage: argv.skipImage || false,
      skipMp3: argv.skipMp3 || false
    });

    let complete = false;

    if (argv.skip) {
      complete = piece.checkCompleteness();
    }
    if (!complete) {
      await piece.generateFolderHierarchy();
      await piece.generateInfo();
      await piece.generateSources();
    } else {
      console.log('Skipping', parsed.label, ': already complete');
    }
  }

  process.exit();
};

main();