import os, json

OUT = '../songs'
PATH = '../../weimar-jazz-database/songs/'


def get_year(s):
    '''
    Returns year from strings such as 1956-10-26, 24-25.10.1966, September 1964
    '''
    def valid_year(yy):
        try:
            y = int(yy)
            return y >= 1900 and y <= 2100
        except:
            return False

    if valid_year(s[:4]):
        return s[:4]
    if valid_year(s[-4:]):
        return s[-4:]
    return None


class WritingClass() :

    def __init__(self) :
        self.keys = { "maj": "major",
                      "min": "minor",
                      "chrom": "chromatic",
                      "dor": "dorian",
                      "blues": "blues",
                      "mix": "mix"}
    
    def write_to_file(self, data, mode, performer='', melid=''):
        if not os.path.isdir(f"{OUT}"):
            os.mkdir(f"{OUT}")
        if not os.path.isdir(f"{OUT}/{melid:04}"):
            os.mkdir(f"{OUT}/{melid:04}")
        # Write dez file
        if mode == "-d":
            file = f'{OUT}/{melid:04}/{melid:04}.dez'
        # Write info.json
        elif mode == "-i":
            file = f'{OUT}/{melid:04}/piece.json'
        # Write synchro.json
        elif mode == "-s":
            file = f'{OUT}/{melid:04}/synchro.json'
        # print('<==', file)
        with open(file, 'w') as f:
            json.dump(data, f, indent=2, sort_keys=True)
    
    def create_dez_file(self, labels, meta_date, meta_producer, meta_title):
        dez_file = dict(
            {  
                "labels": labels,

                "meta":{
                    "date": meta_date,
                    "producer": meta_producer,
                    "title": meta_title,
                    
                    "layout": [
                        { "filter" : { "type": "Chorus" }, "style" : {"line": "top.1"} },
                        { "filter" : { "type": "Structure" }, "style" : {"line": "top.2" } },
                        { "filter" : { "type": "Phrase" }, "style" : {"line": "top.3", "color": "#e0e0ff"} },

                        { "filter" : { "type": "Harmony" }, "style" : {"line": "bot.1"} },
                        { "filter" : { "type": "Pattern" }, "style" : {"line": "bot.2"}  }
                    ]
                }
            })
        return dez_file
    


    def get_metadata(self, melid, trackid, df_soloinfo, df_trackinfo, yt, upbeat):

        df_metadata = df_soloinfo[df_soloinfo['melid'] == melid]
        df_metadata = df_metadata[df_metadata['trackid'] == trackid]
        try:
            track_info = df_trackinfo[df_trackinfo['trackid'] == trackid]
            lineup = track_info['lineup'].values[0]
            recording_date = track_info['recordingdate'].values[0]
            music_brains_id = track_info['mbzid'].values[0]
            yt_info = yt[yt['melid'] == melid]
            yt_id = yt_info['youtube_id'].values[0]
            solo_start = yt_info['solo_start_sec'].values[0]
            solo_end = yt_info['solo_end_sec'].values[0]
        except IndexError:
            lineup = ""
            recording_date = ""
            music_brains_id = ""
            yt_id = ""
            solo_start = ""
            solo_end = ""
        metadata = {

            "id": f"wjd-{melid:04}",
            "analysis:default": f"{melid:04}.dez",
    
            # Score, Analysis
            "sources": [
                {
                    "score": f"{PATH}/{melid:04}/{melid:04}.mei",
                    "license" : "ODbL-1.0"
                },
                {
                    "analysis":  f"{PATH}/{melid:04}/{melid:04}.dez",
                }
            ],
    
            # Metadata
            "opus": {
                "title" : df_metadata['title'].values[0],
                "contributors":
                {
                "artist" : df_metadata['performer'].values[0],
                "performer" : lineup
                },
                "opus": f"wjd-{melid:04}",
                "ref:isbn": "9783959831246",
                "ref:wjd": melid,
                "instrument" : df_metadata['instrument'].values[0],
                "style" : "jazz:solo:" + df_metadata['style'].values[0].lower(),
                "tempo:bpm" : df_metadata['avgtempo'].values[0],
                    # "tempo:class" : df_metadata['tempoclass'].values[0].lower(),  # redondant avec tempo:bpm
                "tempo:feel" : df_metadata['rhythmfeel'].values[0].lower(),
                "meter" : df_metadata['rhythmfeel'].values[0].lower(), # TMP, should be done in front
                "key" : self.key(df_metadata['key'].values[0])
            },
            
            "time-signature" : df_metadata['signature'].values[0],
            "grid" : df_metadata['chord_changes'].values[0]
                
        }
        if int(upbeat) != 0 : metadata["time-signature-upbeat"] = int(upbeat)

        if lineup:
            metadata['opus']['contributors']['performers'] = lineup
            metadata['opus'].update({
                "recording:date": recording_date,
                "ref:musicbrainz:track": music_brains_id,
            })
            year = get_year(recording_date)
            if year:
                metadata['opus']['year'] = year

        if yt_id:
            metadata['sources'] += [{
                "video:yt" : yt_id,
                "date:wjd-solo-start": solo_start,
                "date:wjd-solo-end": solo_end,
                "synchro": f"{PATH}/{melid:04}/synchro.json",
            }]

        return metadata

    def key(self, key) :
        if not key : return ""
        res = key
        keyList = key.split("-")
        if len(keyList) > 1 :
            word = keyList[1]
            res = key.replace(word, self.keys[word.lower()]) \
                     .replace("-", " ")
        return res

