'''
Converts from .krn to .musicxml, fixing clefs for string quartets
'''

from music21 import *
import sys

def change_clef(part, cl):
    m1 = part.measure(1)
    for elt in m1:
        if isinstance(elt, clef.Clef):
            break
    m1.remove(elt)
    m1.insert(0, cl)
    

def krn_to_mxl(f, ff):    
    print('<==', f)
    s = converter.parse(f)
    change_clef(s.parts[2], clef.AltoClef())
    change_clef(s.parts[3], clef.BassClef())

    print('==>', ff)
    s.write('musicxml', ff)

if __name__ == '__main__':
    krn_to_mxl(sys.argv[1], sys.argv[2])