
### Bach, WTC I

class BachFuguesWTC1(JsonCorpus):

    def parse_keys(self, filename):
        # input may be 847, bwv847
        bwv = int(filename.replace('bwv', ''))
        key = 'bwv%d' % bwv

        # 847 ==> 2
        num = bwv - 845

        return {
            'bwv': '%s' % bwv,
            'num': num,
            KEY: key,
            KEYS: [key, str(num), TEMPLATE],
        }

