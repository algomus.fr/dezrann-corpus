{
  "corpus": {
    "availability": "The OpenScore Lieder corpus consists of over 1,300 songs from the long nineteenth century. The collection is available to play online at [musescore.com](https://musescore.com/openscore-lieder-corpus) and is also [available for download](https://github.com/OpenScore/Lieder). For more on the score collection see [(Gotham and Jonas 2021)](https://doi.org/10.1145/3273024.3273026) or [this magazine piece](http://www.sparksandwirycries.org/magazine/2020/8/18/mark-gotham-an-ode-to-digital-scores-for-singers). This Dezrann collection presents a subset of the scores by women composers, including harmonic analyses published on the [‘When in Rome’ meta-corpus](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus) reported in [Gotham et al. 2023a](https://transactions.ismir.net/articles/10.5334/tismir.165). The corpus uses measure maps ([Gotham et al., 2023b](https://doi.org/10.1145/3625135.3625136)) to improve annotation interoperability.",
    "availability:de": "Der OpenScore Liederkorpus besteht aus über 1.300 Liedern aus dem erweiterten 19. Jahrhundert. Die Sammlung ist online auf [musescore.com](https://musescore.com/openscore-lieder-corpus) verfügbar und kann auch [heruntergeladen werden](https://github.com/OpenScore/Lieder). Dieser Dezrann-Korpus präsentiert einen Ausschnitt der Noten von Komponistinnen, einschließlich harmonischer Analysen, die mit dem [‘When in Rome’ Metakorpus](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus) veröffentlicht wurden und in [Gotham et al. 2023a](https://transactions.ismir.net/articles/10.5334/tismir.165) beschrieben sind. Der Korpus verwendet Measure Maps ([Gotham et al., 2023b](https://doi.org/10.1145/3625135.3625136)) für verbesserte Interoperabilität der Annotationen.",
    "availability:el": "Το OpenScore Lieder corpus αποτελείται από πάνω από 1.300 τραγούδια από τη μακρά περίοδο του 19ου αιώνα. Η συλλογή είναι διαθέσιμη για αναπαραγωγή online στο [musescore.com](https://musescore.com/openscore-lieder-corpus) και μπορεί επίσης να [κατεβάσετε](https://github.com/OpenScore/Lieder). Για περισσότερες πληροφορίες σχετικά με τη συλλογή παρατίθεται η εργασία των [(Gotham and Jonas 2021)](https://doi.org/10.1145/3273024.3273026) ή [αυτό το άρθρο στο περιοδικό](http://www.sparksandwirycries.org/magazine/2020/8/18/mark-gotham-an-ode-to-digital-scores-for-singers). Αυτή η συλλογή της Dezrann παρουσιάζει ένα υποσύνολο των παρτιτούρων από γυναίκες συνθέτες, συμπεριλαμβανομένων αρμονικών αναλύσεων που δημοσιεύτηκαν στο [‘When in Rome’ meta-corpus](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus) που αναφέρεται στο [Gotham et al. 2023a](https://transactions.ismir.net/articles/10.5334/tismir.165). Ο κορπός χρησιμοποιεί χάρτες μέτρων ([Gotham et al., 2023b](https://doi.org/10.1145/3625135.3625136)) για να βελτιώσει τη συμβατότητα των σημειώσεων.",
    "availability:fr": "Le corpus [OpenScore Lieder](https://github.com/OpenScore/Lieder) comprend plus de 1 300 lieder du XIXᵉ siècle. ([(Gotham et Jonas 2021)](https://doi.org/10.1145/3273024.3273026), [The Art Song Magazine](http://www.sparksandwirycries.org/magazine/2020/8/18/mark-gotham-an-ode-to-digital-scores-for-singers)). La collection est disponible sur [musescore.com](https://musescore.com/openscore-lieder-corpus) et peut également être [téléchargée](https://github.com/OpenScore/Lieder). Ce corpus Dezrann présente des lieder composées par des femmes, avec des analyses harmoniques publiées sur le [méta-corpus 'When in Rome'](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus). Le corpus utilise les *measure maps* ([Gotham et al., 2023](https://doi.org/10.1145/3625135.3625136)) pour améliorer l'interopérabilité des annotations.",
    "availability:hr": "Korpus The OpenScore Lieder obuhvaća preko 1300 solo pjesama dugog 19. stoljeća. Zbirka je dostupna za online reprodukciju na [musescore.com](https://musescore.com/openscore-lieder-corpus), a moguće je i [preuzimanje](https://github.com/OpenScore/Lieder). Za više informacija o zbirci vidi: [(Gotham i Jonas 2021)](https://doi.org/10.1145/3273024.3273026) ili [Gothamov članak](http://www.sparksandwirycries.org/magazine/2020/8/18/mark-gotham-an-ode-to-digital-scores-for-singers). Ova Dezrannova zbirka je podskup partitura različitih skladateljica, a uključuje i harmonijske analize objavljene na [‘When in Rome’ meta-korpusu](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus), a predstavljene u: [Gotham i suradnici, 2023a](https://transactions.ismir.net/articles/10.5334/tismir.165). Korpus koristi metodu mjerenja razmaka između taktova („measure maps“) ([Gothama i njegovih suradnika, 2023b](https://doi.org/10.1145/3625135.3625136)) za poboljšanje interoperativnosti anotacija.",
    "availability:it": "Il corpus OpenScore Lieder consiste in oltre 1.300 lieder del lungo XIX secolo. La collezione può essere visualizzata e ascoltata online su [musescore.com](https://musescore.com/openscore-lieder-corpus) ed è anche disponibile per il [download](https://github.com/OpenScore/Lieder). Per ulteriori informazioni sulla raccolta di spartiti, vedere [(Gotham and Jonas 2021)](https://doi.org/10.1145/3273024.3273026) o [questo articolo di rivista](http://www.sparksandwirycries.org/magazine/2020/8/18/mark-gotham-an-ode-to-digital-scores-for-singers). Questa collezione Dezrann presenta un sottoinsieme delle partiture di compositrici donne, include un'analisi armonica pubblicata sul [meta-corpus ‘When in Rome’](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus) e riportata in [Gotham et al. 2023a](https://transactions.ismir.net/articles/10.5334/tismir.165). Il corpus utilizza la measure map (mappa delle battute) ([Gotham et al., 2023b](https://doi.org/10.1145/3625135.3625136)) per migliorare l'interoperabilità delle annotazioni.",
    "availability:sl": "Korpus OpenScore Lieder obsega več kot 1300 pesmi iz dolgega devetnajstega stoletja. Zbirka je dostopna za predvajanje na spletu na [musescore.com](https://musescore.com/openscore-lieder-corpus) in jo je mogoče tudi [prenesti](https://github.com/OpenScore/Lieder). Za več informacij o zbirki partitur si oglejte [(Gotham in Jonas 2021)](https://doi.org/10.1145/3273024.3273026) ali [dotični prispevek](http://www.sparksandwirycries.org/magazine/2020/8/18/mark-gotham-an-ode-to-digital-scores-for-singers). Zbirka na Dezrannu vsebuje podmnožico not ženskih skladateljic, vključno s harmoničnimi analizami, objavljenimi v [‘When in Rome’ meta-korpusu](https://github.com/MarkGotham/When-in-Rome/tree/master/Corpus/OpenScore-LiederCorpus), ki so podrobneje opisane v [Gotham et al. 2023a](https://transactions.ismir.net/articles/10.5334/tismir.165). Korpus uporablja avtomatizirano merjenje razdalje med taktoma za namen izboljšave interoperabilnosti oznak (*measure maps*)([Gotham et al., 2023b](https://doi.org/10.1145/3625135.3625136)).",
    "genre": "Lied",
    "id": "lieder",
    "image": "https://upload.wikimedia.org/wikipedia/commons/0/0b/Fannymendelssohn-improved.jpg",
    "image:card": "https://ws.dezrann.net/resources/corpus/images/Fanny-Mendelssohn-zoom.jpg",
    "license": "CC0-1.0",
    "motto": "176 Lieder (solo voice, piano) by 19th Century female composers, including Fanny Mendelssohn and  Josephine Lang",
    "motto:de": "176 Lieder (Stimme, Klavier) von Komponistinnen des 19. Jahrhunderts, darunter Fanny Mendelssohn und Josephine Lang",
    "motto:el": "176 Lieder (μονόφωνο, πιάνο) από γυναικείους συνθέτες του 19ου αιώνα, συμπεριλαμβανομένων των Fanny Mendelssohn και Josephine Lang",
    "motto:fr": "176 Lieder (voix et piano) de compositrices du XIXᵉ siècle, dont Fanny Mendelssohn et Josephine Lang",
    "motto:hr": "176 solo pjesama skladateljica 19. stoljeća uključujući Fanny Mendelssohn i Josephine Lang.",
    "motto:it": "176 Lieder (voce solista, pianoforte) di compositrici del XIX secolo, tra cui Fanny Mendelssohn e Josephine Lang",
    "motto:sl": "176 samospevov (solo glas, klavir) skladateljic 19. stoletja, vključno s Fanny Mendelssohn in Josephine Lang",
    "next": "and synchronized audio for the cycle XXX by XXX.",
    "quality:corpus": 3,
    "quality:corpus:metadata": 4,
    "ref": "https://musescore.com/openscore-lieder-corpus",
    "shorttitle": "19th Century Lieder",
    "shorttitle:de": "Lieder des 19. Jahrhunderts",
    "shorttitle:el": "Λίντερ του 19ου αιώνα",
    "shorttitle:fr": "Lieder du XIXᵉ siècle",
    "shorttitle:hr": "Solo pjesme 19. stoljeća",
    "shorttitle:it": "Lieder del XIX secolo",
    "shorttitle:sl": "Samospevi 19. stoletja",
    "showcase": [
      "5101361",
      "5004640",
      "5000464",
      "6012947"
    ],
    "text": "Romantic [lieder](https://en.wikipedia.org/wiki/Lied) consist of solo songs for voice and piano accompaniment. These songs are typically set to poetry or lyrics and are known for their expressive and emotional qualities. In the 19th century, a period when women were often excluded from many areas of music, lieder was one genre where female composers made significant contributions. [Fanny Mendelssohn](https://en.wikipedia.org/wiki/Fanny_Mendelssohn), [Clara Schumann](https://en.wikipedia.org/wiki/Clara_Schumann), [Josephine Lang](https://en.wikipedia.org/wiki/Josephine_Lang), [Louise Reichardt](https://en.wikipedia.org/wiki/Louise_Reichardt), [Augusta Holmès](https://en.wikipedia.org/wiki/Augusta_Holm%C3%A8s) and other women added depth and diversity to the world of lieder, expanding its horizons and contributing to the rich tapestry of 19th-century music.",
    "text:de": "Romantische [Lieder](https://de.wikipedia.org/wiki/Lied) sine für Sologesang mit Klavierbegleitung komponiert. Diese Lieder zielen in der Regel auf Poesie oder Texte ab und zeichnen sich durch ihre ausdrucksstarken und emotionalen Qualitäten aus. Im 19. Jahrhundert, einer Zeit, in der Frauen oft von vielen Bereichen der Musik ausgeschlossen waren, haben Komponistinnen einen bedeutenden Beitrag zum Genre der Lieder geleistet. [Fanny Mendelssohn](https://de.wikipedia.org/wiki/Fanny_Mendelssohn), [Clara Schumann](https://de.wikipedia.org/wiki/Clara_Schumann), [Josephine Lang](https://de.wikipedia.org/wiki/Josephine_Lang), [Louise Reichardt](https://de.wikipedia.org/wiki/Louise_Reichardt), [Augusta Holmès](https://de.wikipedia.org/wiki/Augusta_Holm%C3%A8s) und viele weitere mehr trugen zu mehr Tiefgang und Diversität des Kunstliedes bei und erweiterten ihren Horizont.",
    "text:el": "Οι ρομαντικοί [λίντερ](https://en.wikipedia.org/wiki/Lied) αποτελούνται από μονόφωνα τραγούδια για φωνή και συνοδεία πιάνου. Αυτά τα τραγούδια συνήθως ρυθμίζονται σε ποίηση ή στίχους και είναι γνωστά για την έκφραση και τη συναισθηματική τους ποιότητα. Κατά τον 19ο αιώνα, περίοδο κατά την οποία οι γυναίκες συχνά αποκλείονταν από πολλούς τομείς της μουσικής, τα λίντερ ήταν ένα είδος όπου οι γυναίκες συνθέτες έκαναν σημαντικές συνεισφορές. Η [Fanny Mendelssohn](https://en.wikipedia.org/wiki/Fanny_Mendelssohn), η [Clara Schumann](https://en.wikipedia.org/wiki/Clara_Schumann), η [Josephine Lang](https://en.wikipedia.org/wiki/Josephine_Lang), η [Louise Reichardt](https://en.wikipedia.org/wiki/Louise_Reichardt), η [Augusta Holmè](https://en.wikipedia.org/wiki/Augusta_Holm%C3%A8s) και άλλες γυναίκες πρόσθεσαν πλούτο και ποικιλία στον κόσμο των λίντερ, διευρύνοντας τις οριζόντιες του και συνcontributing to the rich tapestry of 19th-century music.",
    "text:fr": "Les [lieder romantiques](https://en.wikipedia.org/wiki/Lied) sont des chansons pour voix accompagnées d'un piano. Ces compositions mettent en musique des poèmes et sont réputées pour leurs qualités expressives et émotionnelles. Au XIXᵉ siècle, une période où les femmes étaient souvent exclues de nombreux domaines de la musique, les lieder constituaient l'un des rares genres où les compositrices ont apporté des contributions significatives. [Fanny Mendelssohn](https://fr.wikipedia.org/wiki/Fanny_Mendelssohn), [Clara Schumann](https://fr.wikipedia.org/wiki/Clara_Schumann), [Josephine Lang](https://fr.wikipedia.org/wiki/Josephine_Caroline_Lang), Louise Reichardt, [Augusta Holmès](https://fr.wikipedia.org/wiki/Augusta_Holm%C3%A8s) et d'autres femmes ont enrichi le monde des lieder et contribué à la diversité de la musique du XIXᵉ siècle.",
    "text:hr": "Skladbe za glas i klavirsku pratnju napisane u razdoblju glazbenog romantizma imenujemo solo pjesme. U vrijeme kada su žene bile isključene iz brojnih područja javnog života te im nije priličilo bilo kakvo umjetničko stvaralaštvo, je specifična namjena solo pjesama za intimna okruženja dopuštala da se u njoj okušaju i pripadnice drugog spola. K obliku solo pjesme su značajno doprinesle [Fanny Mendelssohn](https://en.wikipedia.org/wiki/Fanny_Mendelssohn), [Clara Schumann](https://en.wikipedia.org/wiki/Clara_Schumann), [Josephine Lang](https://en.wikipedia.org/wiki/Josephine_Lang), [Louise Reichardt](https://en.wikipedia.org/wiki/Louise_Reichardt), [Augusta Holmè](https://en.wikipedia.org/wiki/Augusta_Holm%C3%A8s) i druge.",
    "text:it": "I [lieder](https://it.wikipedia.org/wiki/Lied) romantici consistono in canzoni soliste per voce e accompagnamento pianistico. Queste canzoni hanno tipicamente un'ambientazione data da testi poetici e sono conosciute per le loro qualità espressive ed emotive. Nel XIX secolo, un periodo in cui le donne erano spesso escluse da molte aree della musica, i lieder erano uno dei generi in cui le compositrici fecero significative contribuzioni. [Fanny Mendelssohn](https://it.wikipedia.org/wiki/Fanny_Mendelssohn), [Clara Schumann](https://it.wikipedia.org/wiki/Clara_Schumann), [Josephine Lang](https://it.wikipedia.org/wiki/Josephine_Lang), [Louise Reichardt](https://it.wikipedia.org/wiki/Louise_Reichardt), [Augusta Holmès](https://it.wikipedia.org/wiki/Augusta_Holm%C3%A8s) e altre donne arricchirono il mondo dei lieder, ampliandone gli orizzonti e contribuendo al ricco tessuto della musica del XIX secolo.",
    "text:sl": "Sklabe za glas in klavirsko spremljavo iz obdobja romantike imenujemo [samospevi](https://en.wikipedia.org/wiki/Lied). Četudi je bilo 19. stoletje obdobje, ko so bile ženske (še) pogosto izključene iz večine glasbenih dejavnosti, so skladateljice, kot so [Fanny Mendelssohn](https://en.wikipedia.org/wiki/Fanny_Mendelssohn), [Clara Schumann](https://en.wikipedia.org/wiki/Clara_Schumann), [Josephine Lang](https://en.wikipedia.org/wiki/Josephine_Lang), [Louise Reichardt](https://en.wikipedia.org/wiki/Louise_Reichardt), [Augusta Holmè](https://en.wikipedia.org/wiki/Augusta_Holm%C3%A8s) in druge s svojimi samospevi prispevale k obogaditvi palete glasbe 19. stoletja.",
    "title": "19th Century Lieder from female composers",
    "title:de": "Kunstlieder von Komponistinnen des 19. Jahrhunderts",
    "title:el": "Λίντερ του 19ου αιώνα από γυναικείους συνθέτες",
    "title:fr": "Compositrices et Lieder du XIXᵉ siècle",
    "title:hr": "Solo pjesme skladateljica 19. stoljeća",
    "title:it": "Lieder di compositrici del XIX secolo",
    "title:sl": "Samospevi skladateljic 19. stoletja",
    "year": ""
  },
  "pieces": {
    "5834392": {
      "analysis:default": "analysis_dez_format.dez",
      "opus": {
        "collection": "4 Mélodies",
        "contributors": {
          "composer": "Marie Jaëll"
        },
        "id": "OpenScore/Lieder/5834392",
        "key": "Ab major",
        "measure-map": "5834392.mm.json",
        "meter": "2/4",
        "ref:imslp#": "511349",
        "ref:musescore": "openscore-lieder-corpus/scores/5834392",
        "ref:wikipedia": "Marie_Jaëll",
        "title": "À toi",
        "year": "1846-1925"
      },
      "quality:annotation": "3",
      "quality:audio": "4",
      "quality:audio:synchro": "3",
      "quality:metadata": "3",
      "quality:musical-time": "3",
      "quality:score": "4",
      "sources": [
        {
          "contributors": {
            "editor": "OpenScore Lieder",
            "license": "CC0-1.0",
            "ref": "https://doi.org/10.17613/1my2-dm23"
          },
          "measure-map": "GEN",
          "name": "OpenScore Lieder",
          "score": "https://github.com/MarkGotham/When-in-Rome/raw/refs/heads/master/Corpus/OpenScore-LiederCorpus//Jaëll,_Marie/4_Mélodies/1_À_toi/lc5834392.mscz"
        },
        {
          "analysis": "https://github.com/MarkGotham/When-in-Rome/raw/refs/heads/master/Corpus/OpenScore-LiederCorpus//Jaëll,_Marie/4_Mélodies/1_À_toi/analysis_dez_format.dez"
        }
      ]
    },
    "5837811": {
      "analysis:default": "analysis_dez_format.dez",
      "opus": {
        "collection": "4 Mélodies",
        "contributors": {
          "composer": "Marie Jaëll"
        },
        "id": "OpenScore/Lieder/5837811",
        "key": "B major",
        "measure-map": "5837811.mm.json",
        "meter": "4/4, 3/4",
        "ref:imslp#": "511349",
        "ref:musescore": "openscore-lieder-corpus/scores/5837811",
        "ref:wikipedia": "Marie_Jaëll",
        "title": "Éternel amour",
        "year": "1846-1925"
      },
      "quality:annotation": "3",
      "quality:audio": "4",
      "quality:audio:synchro": "3",
      "quality:metadata": "3",
      "quality:musical-time": "3",
      "quality:score": "4",
      "sources": [
        {
          "contributors": {
            "editor": "OpenScore Lieder",
            "license": "CC0-1.0",
            "ref": "https://doi.org/10.17613/1my2-dm23"
          },
          "measure-map": "GEN",
          "name": "OpenScore Lieder",
          "score": "https://github.com/MarkGotham/When-in-Rome/raw/refs/heads/master/Corpus/OpenScore-LiederCorpus//Jaëll,_Marie/4_Mélodies/2_Éternel_amour/lc5837811.mscz"
        },
        {
          "analysis": "https://github.com/MarkGotham/When-in-Rome/raw/refs/heads/master/Corpus/OpenScore-LiederCorpus//Jaëll,_Marie/4_Mélodies/2_Éternel_amour/analysis_dez_format.dez"
        }
      ]
    },
    "5840072": {
      "opus": {
        "collection": "4 Mélodies",
        "contributors": {
          "composer": "Marie Jaëll"
        },
        "id": "OpenScore/Lieder/5840072",
        "ref:imslp#": "511349",
        "ref:musescore": "openscore-lieder-corpus/scores/5840072",
        "ref:wikipedia": "Marie_Jaëll",
        "title": "Les petits oiseaux",
        "year": "1846-1925"
      },
      "quality:annotation": "3",
      "quality:audio": "4",
      "quality:audio:synchro": "3",
      "quality:metadata": "3",
      "quality:musical-time": "3",
      "quality:score": "4",
      "sources": [
        {
          "contributors": {
            "editor": "OpenScore Lieder",
            "license": "CC0-1.0",
            "ref": "https://doi.org/10.17613/1my2-dm23"
          },
          "measure-map": "GEN",
          "name": "OpenScore Lieder",
          "score": "https://github.com/MarkGotham/When-in-Rome/raw/refs/heads/master/Corpus/OpenScore-LiederCorpus//Jaëll,_Marie/4_Mélodies/3_Les_petits_oiseaux/lc5840072.mscz"
        }
      ]
    },
    "5840228": {
      "opus": {
        "collection": "4 Mélodies",
        "contributors": {
          "composer": "Marie Jaëll"
        },
        "id": "OpenScore/Lieder/5840228",
        "measure-map": "5840228.mm.json",
        "meter": "2/4, 3/8",
        "ref:imslp#": "511349",
        "ref:musescore": "openscore-lieder-corpus/scores/5840228",
        "ref:wikipedia": "Marie_Jaëll",
        "title": "Le bonheur s’effeuille et passe",
        "year": "1846-1925"
      },
      "quality:annotation": "3",
      "quality:audio": "4",
      "quality:audio:synchro": "3",
      "quality:metadata": "3",
      "quality:musical-time": "3",
      "quality:score": "4",
      "sources": [
        {
          "contributors": {
            "editor": "OpenScore Lieder",
            "license": "CC0-1.0",
            "ref": "https://doi.org/10.17613/1my2-dm23"
          },
          "measure-map": "GEN",
          "name": "OpenScore Lieder",
          "score": "https://github.com/MarkGotham/When-in-Rome/raw/refs/heads/master/Corpus/OpenScore-LiederCorpus//Jaëll,_Marie/4_Mélodies/4_Le_bonheur_s’effeuille_et_passe/lc5840228.mscz"
        }
      ]
    }
  },
  "settings": {
    "access": "public"
  },
  "template": {
    "opus": {
      "..quality:annotation": "3",
      "..quality:audio": "4",
      "..quality:audio:synchro": "3",
      "..quality:metadata": "3",
      "..quality:musical-time": "2",
      "..quality:score": "4"
    }
  }
}