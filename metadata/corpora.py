from corpus import JsonCorpus, ExternalCorpus, ALL, TEMPLATE, KEY, KEYS
import glob

for f_corpus in glob.glob('metadata/*.py'):
    if 'corpora' in f_corpus:
        continue
    print(f_corpus)
    exec(open(f_corpus).read())

print('=========')

### Corpora list

AVAILABLE_CORPORA = [
    JsonCorpus('test', 'metadata/test-corpus.json'),
    JsonCorpus('minimal', 'metadata/minimal.json'),

    ## Baroque
    EcolmLute('ecolm-lute', 'metadata/ecolm-lute.json', template=True),
    CorelliTrioSonatas('corelli-trio-sonatas', '../corelli-trio-sonatas/corelli-trio-sonatas.json', template=True),
    TelemannFluteFantaisies('telemann-flute-fantaisies', 'metadata/telemann-flute-fantaisies.json', template=True),
    BachFuguesWTC1('bach-fugues', 'metadata/bach-fugues.json', template=True),
    # VivaldiQuattroStagioni(),

    ## Classical
    MozartPianoSonatas('mozart-piano-sonatas', 'metadata/mozart-piano-sonatas.json', template=True),
    MozartStringQuartets('mozart-string-quartets', 'metadata/mozart-string-quartets.json', template=True),

    JsonCorpus('classical-symphonies', 'metadata/classical-symphonies.json', subcorpora = ['mozart-symphonies', 'haydn-symphonies', 'beethoven-symphonies']),
    MozartSymphonies('mozart-symphonies', 'metadata/mozart-symphonies.json', template=True),
    HaydnSymphonies('haydn-symphonies', 'metadata/haydn-symphonies.json', template=True),
    BeethovenSymphonies('beethoven-symphonies', 'metadata/beethoven-symphonies.json', template=True),

    ## Romantic
    JsonCorpus('schubert-winterreise', 'metadata/schubert-winterreise.json', extra=['metadata/schubert-winterreise-pieces.json'], template=True),
    JsonCorpus('openscore-lieder', 'metadata/openscore-lieder.json', extra=['metadata/openscore-lieder-corpus.json']),
    JsonCorpus('collabscore', 'metadata/collabscore-saintsaens.json', template=True),
    JsonCorpus('supra', 'metadata/supra.json', extra=['metadata/supra-pieces.json']),
    
    ## Folk, Jazz, Pop, Other
    JsonCorpus('weimar-jazz', 'metadata/weimar-jazz-songs.json', extra=['metadata/weimar-jazz.json']),
    SlovenianFolkSongBallads('slovenian-folk-song-ballads', 'metadata/slp.json', extra=['metadata/slp-corpus.json'], template=True),
    JsonCorpus('erkomaishvili', 'metadata/erkomaishvili.json', extra=['metadata/erkomaishvili-pieces.json'], template=True),
    JsonCorpus('mcflow', '../toplines/corpus/mcflow.json'),
    JsonCorpus('brutal-death', '../brutal-death-tabs/brutal-death-metal.json', template=True),
    ### 
    # JsonCorpus('edmus', 'metadata/edmus.json'),
    # JsonCorpus('mathieu', 'metadata/mathieu.json'),
    PremusicStimuli('premusic', 'metadata/premusic-stimuli.json', template=True),
]

### Tags

CORPORA_TAGS = {
    'tismir': ('🌏 Public corpora', [
        'bach-fugues',
        'mozart-piano-sonatas',
        'mozart-string-quartets',
        'classical-symphonies',
        'schubert-winterreise',
        'openscore-lieder',
        'weimar-jazz',
        'supra',
        'slovenian-folk-song-ballads',
        'erkomaishvili',
    ]),
    'ongoing': ('🚧 Private/ongoing corpora', [
        'ecolm-lute',
        'corelli-trio-sonatas',
        'telemann-flute-fantaisies',
        'collabscore',
        'mcflow',
        'brutal-death',
        'premusic',
    ]),
    'test': ('⚙️ Test corpora', [
        'test',
        'minimal',
    ])
}