
class MozartSymphonies(JsonCorpus):

    def parse_keys(self, key):
        # k999.9
        if '.' not in key:
            return None
        key_piece = key.split('.')[0]
        opus = key_piece.replace('k', 'K.') # K.999
        mvnt = key.split('.')[1]

        return {
            KEY: key,
            KEYS: [key, key_piece, TEMPLATE],
            'k': key_piece,
            'opus': opus,
            'mvt': mvnt
        }

class HaydnSymphonies(JsonCorpus):

    def parse_keys(self, key):
        # hob999.9
        if '.' not in key:
            return None
        key_piece = key.split('.')[0]
        opus = key_piece.replace('hob', 'Hob.I:')
        mvt = key.split('.')[1]

        return {
            KEY: key,
            KEYS: [key, key_piece, TEMPLATE],
            'hob': key_piece,
            'opus': opus,
            'mvt': mvt
        }

class BeethovenSymphonies(JsonCorpus):

    def parse_keys(self, key):
        # op999.9
        if '.' not in key:
            return None
        key_piece = key.split('.')[0]
        opusnum = key_piece.replace('op', '')
        mvt = key.split('.')[1]

        return {
            KEY: key,
            KEYS: [key, key_piece, TEMPLATE],
            'opusnum': opusnum,
            'mvt': mvt
        }
