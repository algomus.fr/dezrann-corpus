# Corelli Trio Sonatas

class CorelliTrioSonatas(JsonCorpus):

    def parse_keys(self, key):
        # input is 1.1.1
        key_piece = key.split('.')
        assert(len(key_piece) == 3)

        return {
            KEY: 'op' + key,
            KEYS: [key, key_piece[0]+"."+key_piece[1], key_piece[0], TEMPLATE],
            'opusnum': int(key_piece[0]),
            'num': int(key_piece[1]),
            'mvt': int(key_piece[2]),
        }

    def load_items(self):
        self.FILES += filter(lambda key: key.count('.') == 2, self.d['pieces'])

