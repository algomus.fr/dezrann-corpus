### Bach, WTC I

class TelemannFluteFantaisies(JsonCorpus):

    def parse_keys(self, filename):
        twv = filename.replace('twv40:', '')

        # 02 ==> 1
        num = str(int(twv) - 1)

        return {
            'twv': twv,
            'num': num,
            KEY: filename,
            KEYS: [filename, num, twv, TEMPLATE],
        }
