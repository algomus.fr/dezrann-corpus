

class VivaldiQuattroStagioni():

    ID = 'vivaldi-quattro-stagioni'
    SERVER = 'alb:corpus'

    METADATA = { 
        ALL: {
            "contributors.composer": "Antonio Vivaldi",
            "id" : "vivaldi/{KEY}",
            "year": "1718-1720",
            "title": "{piece_title}, {opus}, {mvt}. {movement_title}",
            "piece:title": {
                "spring": "La primavera, Concerto no 1 in E major",
                "summer": "L'estate, Concerto no 2 in G minor",
                "autumn": "L'autumno, Concerto no 3 in F major",
                "winter": "L'inverno, Concerto no 4 in F minor",
            },
            "title:fr": "{piece_title_fr}, {opus}, {mvt}. {movement_title}",
            "piece:title:fr": {
                "spring": "Le printemps, Concerto n°1 en mi majeur",
                "summer": "L'été, Concerto n°2 en sol mineur",
                "autumn": "L'automne, Concerto n°3 en fa majeur",
                "winter": "L'hiver, Concerto n°4 in fa mineur",
            },
            "collection": "Le quattro stagioni",
            "collection:fr": "Les quatre saisons",
            "collection:en": "The four seasons",
            "genre": "Violin Concerto",
            "ref:musicbrainz": "87886dcf-9776-49cb-b6f5-10104da6e42c",
            "ref:wikipedia": "The_Four_Seasons_(Vivaldi)",
            "ref:wikipedia:fr": "Les_Quatre_Saisons",
            "ref:imslp": {
                "spring": "Violin_Concerto_in_E_major,_RV_269_(Vivaldi,_Antonio)",
                "summer": "Violin_Concerto_in_G_minor,_RV_315_(Vivaldi,_Antonio)",
                "autumn": "Violin_Concerto_in_F_major,_RV_293_(Vivaldi, Antonio)",
                "winter": "Violin_Concerto_in_F_minor,_RV_297_(Vivaldi, Antonio)",
            },
            "order": {
                "spring": "1",
                "summer": "2",
                "autumn": "3",
                "winter": "4",
            },
            "opus": {
                TEMPLATE: "op. 8, {VALUE}",
                "spring": "RV 269",
                "summer": "RV 315",
                "autumn": "RV 293",
                "winter": "RV 297",
            },
            "movement:num": "{mvt}",
            "key": {
                "spring": "E major",
                "summer": "G minor",
                "autumn": "F major",
                "winter": "F minor",
            },
            "nickname": {
                "spring": "La primavera",
                "summer": "L'estate",
                "autumn": "L'autumno",
                "winter": "L'inverno",
            },
            "nickname:en": {
                "spring": "Spring",
                "summer": "Summer",
                "autumn": "Autumn",
                "winter": "Winter",
            },
            "nickname:fr": {
                "spring": "Le printemps",
                "summer": "L'été",
                "autumn": "L'automne",
                "winter": "L'hiver",
            },

            "..quality:audio": "4",
            "..quality:audio:synchro": "3",

            "..sources.audios.0.contributors.conductor": "John Harrison",
            "..sources.audios.0.contributors.ensemble": "The Wichita State University Chamber Players",
            "..sources.audios.0.license": "CC-BY-SA-2.0",
            "..sources.audios.0.ref": "https://freemusicarchive.org/",

        },

        "spring-1" : { "movement:title": "Allegro" },
        "spring-2" : { "movement:title": "Largo e pianissimo sempre" },
        "spring-3" : { "movement:title": "Allegro pastorale" },

        "summer-1" : { "movement:title": "Allegro non molto" },
        "summer-2" : { "movement:title": "Adagio e piano - Presto e forte" },
        "summer-3" : { "movement:title": "Presto" },

        "autumn-1" : { "movement:title": "Allegro" },
        "autumn-2" : { "movement:title": "Adagio molto" },
        "autumn-3" : { "movement:title": "Allegro" },

        "winter-1" : { "movement:title": "Allegro non molto" },
        "winter-2" : { "movement:title": "Largo" },
        "winter-3" : { "movement:title": "Allegro" },
    }

    FILES = glob.glob('corpus/vivaldi-four-seasons/synchro/*-[123]*-synchro.json')
    
    def parse_keys(self, filename):
            # input may be spring-1, spring-1.mei, vivaldi-spring-1-synchro.json
            if 'synchro' in filename:
                    filename = '-'.join(filename.split('-')[1:3])
            season = filename.split('-')[0]
            return {
                    KEY: filename,
                    KEYS: [filename, season, ALL],
                    'mvt': filename.split('-')[1]
            }
