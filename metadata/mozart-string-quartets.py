class MozartStringQuartets(JsonCorpus):

    def parse_keys(self, filename):
        # input may be k279.1, k279-1.mscz, k279-1.musicxml, k279-1.mei, k279-1/info.json
        key = filename.replace('.mscz', '').replace('.musicxml', '').replace('.mei', '').replace('/info.json', '')
        key = key.replace('-', '.')
        assert(len(key) in [5, 6])

        key_piece = key.split('.')[0]
        assert(len(key_piece) in [3,4])

        # k279 => K.279
        opus = "K." + key_piece[1:]

        return {
            KEY: key,
            KEYS: [key, key_piece, 'template'],
            'opus': opus,
            'k': int(key_piece[1:]),
            'mvt': int(key.split('.')[1])
        }

    QUARTETS = [
        'k80', 'k155', 'k156', 'k157', 'k158', 'k159', 'k160',
        'k168', 'k169', 'k170', 'k171', 'k172', 'k173',
        'k387', 'k421', 'k428', 'k458', 'k464', 'k465',
        'k499',
        'k575', 'k589', 'k590'
    ]

    THREE_MOVEMENTS = [
        'k155', 'k156', 'k157', 'k158', 'k159', 'k160',
        'k428', 'k458', 'k575'
    ]

    FILES = [ ]

    def load_items(self):
        for quartet in self.QUARTETS:
            mvts = [1, 2, 3] if quartet in self.THREE_MOVEMENTS else [1, 2, 3, 4]
            self.FILES += [ f'{quartet}.{mvt}' for mvt in mvts ]

