

### Weimar Jazz Database

class WeimarJazz():

    ID = "weimar-jazz"

    METADATA = {
        ALL: {
            "collection": "Weimar Jazz Database",
            "opus": "wjd-{KEY}",
            "..id": "{KEY}",
            "..composer": "",

            "..quality:audio": "4",
            "..quality:audio:synchro": "3",
            "..sources.audios.0.file": "{KEY}.mp3",
        }
    }

    FILES = glob.glob('../jazz-schubert/songs/*/*/*.musicxml')

    FILES_COPY = [
        ('info.json', './'),
        ('{KEY}.dez', 'analyses/'),
        ('synchro.json', 'sources/audios/{KEY}/')
    ]

    def parse_keys(self, filename):
        key = '/'.join(filename.split('/')[-3:]).split('.')[0]
        return {
            KEY: key,
            KEYS: [key, ALL],
        }

