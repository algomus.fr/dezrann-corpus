
class SlovenianFolkSongBallads(JsonCorpus):

    def parse_keys(self, filename):
        # input may be '252-124.mxl'
        key = filename.replace('.mxl', '')
        keyR = key.replace('--','==')
        type_variant = keyR.split('-')
        assert(len(type_variant) == 2)

        return {
            KEY: key,
            KEYS: [filename, key, TEMPLATE],
            'tunetype': type_variant[0],
            'var': type_variant[1].replace('==', '--'),
        }
