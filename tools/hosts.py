### Configuring Dezrann hosts

ALA = 'dezrann-dev@ala.algomus.net'
ALB = 'alb'

SAVE_OLD = "mv corpus/{ID} corpus-old/{ID}--$(date '+%Y-%m-%d--%H:%M')"
PROMOTE = "mv corpus/sandbox/{ID} corpus/{ID}"

DEFAULT_HOST = 'ala'

SERVERS = {
    
    # Internal test server
    'ala': {
        'RSYNC': f'{ALA}:/home/ubuntu/corpus/sandbox',
        'RSYNC_PROD': f'{ALA}:/home/ubuntu/corpus',
        'RSYNC_STATIC': f'{ALA}:/home/ubuntu/static_resources/sandbox/',
        'SAVE_OLD': f'ssh {ALA} "cd ../ubuntu ; {SAVE_OLD}"',
        'PROMOTE': f'ssh {ALA} "cd ../ubuntu ; {PROMOTE}"',
        'PUBLISH': 'https://test-ws.dezrann.net/addNewPiece',
        'RESTART': f'ssh {ALA} "cd /home/ubuntu/docker/dezrann-back; docker-compose restart"'
    },
    
    # Public Dezrann server
    'alb': {
        'RSYNC': f'{ALB}:corpus/sandbox',
        'RSYNC_PROD': f'{ALB}:corpus',
        'RSYNC_STATIC': f'{ALB}:static_resources/sandbox/',
        'SAVE_OLD': f'ssh {ALB} "{SAVE_OLD}"',
        'PROMOTE': f'ssh {ALB} "{PROMOTE}"',
        'PUBLISH': 'http://alb.algomus.net:8080/addNewPiece',
        'RESTART':'ssh dezrann@alb.algomus.net "cd /home/dezrann/dezrann-back/docker; docker-compose restart"'
    },
}