

# Status, documentation
rm -f doc/status.md
cp doc/status.header-md doc/status.md
date "+%Y-%m-%d %H:%M" >> doc/status.md
python tools/dezrann-corpus.py --status --tag tismir
cp -f doc/status.md doc/status-public.md
python tools/dezrann-corpus.py --status --tag ongoing
python tools/dezrann-corpus.py --status --tag test
mv -f doc/status.md doc/status-private.md
mv doc/status-public.md doc/status.md
make -C doc ; make -C doc scp

# reset
python tools/dezrann-corpus.py --restart --go  

# <corpus>.json ==> <corpus>.full.json (for template-based corpus)
python tools/dezrann-corpus.py --all --template --go


# --pub
## python tools/dezrann-corpus.py --all --pub --go

##############################################
# TISMIR, july 2024
python tools/dezrann-corpus.py bach-fugues --pub --go  # 23'
python tools/dezrann-corpus.py mozart-piano-sonatas --pub --go   # 31' 41'
python tools/dezrann-corpus.py mozart-string-quartets --pub --go   # 22'
# python tools/dezrann-corpus.py  --pub --go haydn-symphonies mozart-symphonies beethoven-symphonies  #  60', with many OOM #  20' (Mozart, OOM) 36' (Haydn) 41' (Beethoven)
python tools/dezrann-corpus.py openscore-lieder --pub --go  # 34'
python tools/dezrann-corpus.py slovenian-folk-songs-ballads --pub --go  # 42' 38'

# TISMIR, other corpora
## Winterreise
## SUPRA
## WJD
## Erkomaishvili


##############################################
# CollabScore, july 2024  
python tools/dezrann-corpus.py collabscore --template --go
python tools/dezrann-corpus.py collabscore --pub --go  # 20:00
python tools/dezrann-corpus.py collabscore --prod --down --update-meta --up --go
python tools/dezrann-corpus.py collabscore --prod --corpus --go
python tools/save-synchro.py --up    ## Synchro Collabscore


#>> dezrann@alb:~/corpus$ mv collabscore/ saint-saens
# 19/07/24: non, pas encore, liens statiques ne passent pas

##############################################
# Other corpora, 2024

python tools/dezrann-corpus.py ecolm-lute --pub --go
python tools/dezrann-corpus.py corelli-trio-sonatas  --pub --go    # 16'
python tools/dezrann-corpus.py telemann-flute-fantaisies --pub --go    
python tools/dezrann-corpus.py mcflow --pub --go    
python tools/dezrann-corpus.py premusic --pub --go    

# Update only analyses on a prod corpus ?
# python tools/dezrann-corpus.py corelli-trio-sonatas --update --prod --go

##############################################
## Update all corpus.json/access.json (after moving from corpus/sandbox to corpus/)
python tools/dezrann-corpus.py --all --corpus --prod --go -l 0


