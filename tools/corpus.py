
import glob
import json
import os
from pathlib import Path

from rich import print
from tools import exec, str_stats_distribution, MetaDict, pluralize, render_quality
from collections import defaultdict
import livestats
import dezrann_api

ALL = 'ALL'
KEY = 'KEY'
KEYS = 'KEYS'
VALUE = 'VALUE'
TEMPLATE = 'template'

METADATA_PATH = 'metadata'
INFO_JSON = 'info.json'
FROM_ARCHIVE = 'from-archive'

SOURCES = {
    'score': (
        'score',
        'Scores'),
    'measure-map': (
        'measure map',
        'Measure maps as defined by <https://doi.org/10.1145/3625135.3625136>,'),
    'analysis': (
        'analyse',
        'Analyses, in `.dez` format, as described on <https://doc.dezrann.net/dez-format>'),
    'audio': (
        'recording',
        'Audio recordings'),
    'video': (
        'video',
        'Video recordings'),
    'synchro': (
        'synchronization',
        'Sychronizations between musical time and audio time, as described on <https://doc.dezrann.net/synchro>')
}

class Corpus():

    def __str__(self):
        return f'<{self.__class__.__name__} {self.ID} ({len(self.FILES)})>'

    def get_keys(self, f, args):
        f_base = f.replace(f'/{INFO_JSON}', '').replace('.musicxml', '').replace('.xml', '').replace('.krn', '')
        f_basebase = os.path.basename(f_base)
        keys = self.parse_keys(f_basebase)
        if keys is None:
            return None
        key = keys[KEY]
        print(f"[yellow]{key}", end=' ')
        if args.verbose > 0:
            k = keys.copy()
            del k[KEY]
            del k[KEYS]
            print(f'← <{" ".join(keys[KEYS])}> {k}')
        return keys

    def parse_keys(self, filename):
        raise

    def save(self):
        pass

    def settings(self, key):
        if 'settings' not in self.d:
            return None
        if key not in self.d['settings']:
            return None
        return self.d['settings'][key]

class JsonCorpus(Corpus):


    def __init__(self, ID, METADATA_JSON, extra=None, template=False, subcorpora=None):
        self.ID = ID
        self.template = template
        self.extra = extra
        
        if self.template:
            self.metadata_template = METADATA_JSON
            self.metadata = f'{METADATA_PATH}/{ID}.full.json'
        else:
            self.metadata_template = None
            self.metadata = METADATA_JSON
        self.FILES = []
        self.pieces = {}
        self.subcorpora = subcorpora

    def load(self, args):
        f_metadata = self.metadata_template if (self.template and args.template) else self.metadata
        print('<==', f_metadata)
        self.d = json.load(open(f_metadata))

        if self.extra:
            if (self.template and args.template) or (not self.template):
                for f_extra in self.extra:
                    print('<==', f_extra)
                    extra = json.load(open(f_extra))
                    for k, v in extra.items():
                        if k not in self.d:
                            self.d[k] = {}
                        print('   ', k)
                        self.d[k].update(v)

        if not 'pieces' in self.d:
            print('[red] No pieces ?')
        else:
          for piece_id, piece in self.d['pieces'].items():
            self.pieces[piece_id] = piece

    def load_items(self):
        self.FILES = list(self.pieces.keys())
        return

    def load_from_subcorpora(self, corpora):
        '''Load pieces from subcorpora'''
        if self.subcorpora:
            for c in self.subcorpora:
                print(f'[purple]<== {c}')
                sub = { c.ID: c for c in corpora }[c]
                self.pieces.update(sub.pieces)
                self.d['pieces'] = self.pieces

    def __getitem__(self, piece_id):
        return self.pieces[piece_id]

    def __setitem__(self, piece_id, piece_data):
        self.pieces[piece_id] = piece_data
        self.d['pieces'][piece_id] = piece_data

    def save(self, target=None):
        if not target:
            target = self.metadata
            if not self.template:
                return
        print(f'[yellow]==> {target}')
        with open(target, 'w', encoding="utf-8") as f:
            json.dump(self.d, f, ensure_ascii=False, indent=2, sort_keys=True)

    def parse_keys(self, filename):
        key = filename
        return {
            KEY: key,
            KEYS: [key, TEMPLATE],
        }

    def status(self):

        stats = defaultdict(str)
        stats['ID'] = self.ID
        stats['ID_'] = f'<a href="#{self.ID}"><b>{self.ID}</b></a>'

        ### Status
        for field in ['availability', 'status']:
            if field in self.d['corpus']:
                stats[field] = self.d['corpus'][field]
            else:
                stats[field] = '?'
        stats['status_'] = stats['status'][0]

        ### Stats on pieces
        if len(self.pieces):
            stats['pieces'] = f'{len(self.pieces)} pieces'
            stats['pieces_'] = f'{len(self.pieces)}'
        else:
            stats['pieces'] = '⚠️ No pieces ?'
            stats['pieces_'] = '⚠️'

        ### Stats on sources
        nb = defaultdict(int)
        
        ### From server
        try:
            serv_stats = livestats.corpus_statistics(self.ID)
            for k, v in serv_stats.items():
                stats[f'{k}_on_server'] = f'<span class="onserver">[{v}]</span>'
            stats['sources_s'] = 'On server: ' + ', '.join(f'{v} {k}' for k, v in serv_stats.items())
        except:
            print('!! no livestats',self.ID)
        
        ### From .json
        for piece in self.pieces.values():
            if not 'sources' in piece:
                continue
            for source in piece['sources']:
                if not source:
                    continue
                found = False
                for k, v in SOURCES.items():
                    try:
                        kks = source.keys()
                    except:
                        print('[red]! No keys', k, v)
                        kks = []
                    for kk in kks:
                        if kk == k or kk.startswith(f'{k}:'):
                            nb[k] += 1
                            found = True
                            for kkk in source.keys():
                                if kkk.startswith('quality'):
                                    quality[k][kkk] += 1
                if not found:
                    print('!', source)

        sta = ', '.join([f'{nb[k]} {pluralize(v[0], nb[k])}' for k, v in SOURCES.items() if nb[k]])
        if not sta:
            sta = '⚠️ No sources ?'
        stats['sources'] = sta

        for k, v in SOURCES.items():
            if nb[k]:
                stats[f'{k}_'] = f'{nb[k]}'

        ### Stats on quality (now these fields are per piece)
        quality = defaultdict(lambda: defaultdict(int))

        if 'quality:corpus' in self.d['corpus']:
            quality['corpus'][self.d['corpus']['quality:corpus']] = 1
        if 'quality:corpus:metadata' in self.d['corpus']:
            quality['corpus:metadata'][self.d['corpus']['quality:corpus:metadata']] = 1

        for piece in self.pieces.values():
            for k in piece.keys():
                if k.startswith('quality'):
                    quality[k.replace('quality:', '')][piece[k]] += 1

        s = [f'{k}: {str_stats_distribution(v)}' for k, v in quality.items()]
        if s:
            stats['quality'] = 'Quality: ' + ', '.join(s)
        else:
            stats['quality'] = '⚠️ No quality information'

        for k, v in quality.items():
            stats[f'quality:{k}_'] = render_quality(str_stats_distribution(v, True))

        ### Stats on issues
        label = f'corpus::{self.ID}'
        issues = livestats.issues_statistics(label)
        stats['issues'] = '⚠️ No issues'
        if issues and 'statistics' in issues:
            d = issues['statistics']['counts']
            url = livestats.GITLAB_ISSUES_URL + label
            if d['all']:
                stats['issues'] = f"Issues: [{d['opened']} opened]({url}), {d['closed']} closed"
                stats['issues_'] = f"[{d['opened']}]({url})"

        ### Stats on server
        server = dezrann_api.info(self.ID, host='alb', dry=False, prod=True)
        if not server:
            server = '⚠️ No information on server'
            server_ = '⚠️'
        else:
            server_ = server
            server = 'Rebuilt: ' + server
        stats['build'] = server
        stats['build_'] = server_        

        # Metadata
        if 'corpus' in self.d:
            meta = MetaDict(self.d['corpus'])
        else:
            meta = {'title': '?'}
        stats['title'] = meta['title']

        if 'contributors' in meta:
            meta.update(meta['contributors'])

        if 'maintainer' in meta:
            maint = f'[Maintainers](mailto:{meta["maintainer"]}?subject=[Dezrann]%20{self.ID}%20corpus)'
        else:
            maint = '⚠️ No maintainer'
        stats['maintainer'] = maint

        stats['license'] = meta['license']
        stats['license_'] = '⚠️' if 'Empty' in meta['license'] else '✅'

        # lic = '''  - Licpense: {license}
#  - References: {ref_pub}, <https://dx.doi.org/{ref_doi}>, <{ref}>'''.format_map(meta)

        met = f'[Metadata]({livestats.GITLAB_REPO_URL}/{self.metadata})'
        met += f' ({len(str(self.d))/1000:.0f} KB)'
        stats['metadata'] = met

        if 'image' in meta:
            im = f"<img class='corpus' width='100px' style='float:right;' src='{meta['image']}'>"
        else:
            im = ''

        # Output
        output = im + '''
<a name='{ID}' />

### {ID} » {title}
- <https://www.dezrann.net/explore/{ID}>
- {availability}
- {status}
- Content
  - {pieces}
  - {sources}
  - {sources_s}
  - {quality}
- {metadata}
  - License: {license}
  - {maintainer}
- {issues}
- {build}
        '''.format_map(stats)

        return stats, output

class ArchiveCorpus(JsonCorpus):
    '''
    Archive corpus, from a .zip file
    '''

    def __init__(self, archive):

        self.archive = archive
        name = Path(archive).stem
        print(f'[yellow]### {archive} ==> {name}')
        path = FROM_ARCHIVE
        self.PATH_IN = f'../../../{path}/{name}'

        exec(f'rm -rf {path}/{name}')
        exec(f'mkdir -p {path}')
        exec(f'unzip {self.archive} -d {path}')

        metadata = f'{path}/{name}/{name}.json'
        print(f'[yellow]<== {metadata}')
        super().__init__(name, metadata)

class ExternalCorpus(JsonCorpus):

    '''
    External corpus, data is generated with other scripts
    May be used to handle corpus.json.
    '''
    
    pass

### Init/load corpora

def load_corpora(corpora, args):
    CORPORA = []
    for c in corpora:
        try:
            if hasattr(c, 'load') :
                c.load(args)
            if hasattr(c, 'load_items') :
                c.load_items()
            print('🎶', c)
            CORPORA += [c]
        except IOError as e:
            print('[red]! %s %s' % (c.ID, e))
    
    # Reload corpora with subcorpora
    for c in CORPORA:
        c.load_from_subcorpora(CORPORA)

    return CORPORA
