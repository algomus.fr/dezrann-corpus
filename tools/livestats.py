
import requests
from collections import defaultdict

GITLAB_REPO_URL = 'https://gitlab.com/algomus.fr/dezrann/dezrann-corpus/-/blob/main/'
GITLAB_ISSUES_URL = 'https://gitlab.com/groups/algomus.fr/dezrann/-/issues/?label_name[]='
GITLAB_API_URL = "https://gitlab.com/api/v4"
ACCESS_TOKEN = ""

DEZ_CORPUS_API = 'https://ws.dezrann.net/corpus/%s/recursive'

try:
    from tokens import ACCESS_TOKEN
except:
    pass

def issues_statistics(label):
    """Return /issue_statistics for a given label"""
    headers = {
        "Private-Token": ACCESS_TOKEN
    }
    url = f"{GITLAB_API_URL}/issues_statistics?scope=all&labels={label}"
    response = requests.get(url, headers=headers)
        
    if response.status_code != 200:
        print(f"!{url}: {response.status_code}, {response.text}")

    data = response.json()

    return data

def corpus_statistics(corpus):
    """Return corpus statistics on a live server"""
    
    url = DEZ_CORPUS_API % corpus
    response = requests.get(url)
        
    if response.status_code != 200:
        print(f"!{url}: {response.status_code}, {response.text}")

    try:
        data = response.json()
    except:
        print('!! No json', corpus)
        return {}


    out = defaultdict(int)
    
    for p in data['pieces']:
        sources = p['sources']
        if 'audios' in sources:
            for a in sources['audios']:
                out['audio'] += 1
                for i in a['images']:
                    out[i['type']] += 1

        if 'images' in sources:
            for i in sources['images']:
                out[i['type']] += 1
    
    return out



if __name__ == "__main__":
    print(issues_statistics('corpus::bach-fugues'))
    print(corpus_statistics('bach-fugues'))
       