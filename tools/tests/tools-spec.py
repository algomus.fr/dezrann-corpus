import unittest
from tools import get_file_or_url

NOT_EXISTING_FILE_PATH = '/not/existing/file'
SOURCE_FILE_PATH = '/tmp/file'
DEST_FILE_PATH = '/tmp/dest'

class Test_get_file_or_url(unittest.TestCase):
    
    def setUp(self):
        f = open(SOURCE_FILE_PATH, "a")
        f.write("Now the file has more content!")
        f.close()

    def test_file_source_does_not_exist(self):
        self.assertEqual(get_file_or_url(NOT_EXISTING_FILE_PATH), None)

    def test_file_source_and_target_dont_exist(self):
        self.assertEqual(get_file_or_url(NOT_EXISTING_FILE_PATH, NOT_EXISTING_FILE_PATH), None)
        
    def test_file_source_exists_and_target_does_not(self):
        self.assertEqual(get_file_or_url(SOURCE_FILE_PATH, NOT_EXISTING_FILE_PATH), None)

    def test_file_source_exists(self):
        self.assertEqual(get_file_or_url(SOURCE_FILE_PATH), SOURCE_FILE_PATH)

    def test_file_source_and_target_exist(self):
        self.assertEqual(get_file_or_url(SOURCE_FILE_PATH, DEST_FILE_PATH), DEST_FILE_PATH)

    def test_url_does_not_extst(self):
        self.assertEqual(get_file_or_url('http://unknown.fr/file'), None)
        
    def test_url_and_target_dont_exist(self):
        self.assertEqual(get_file_or_url('http://unknown.fr/file', NOT_EXISTING_FILE_PATH), None)
        
    def test_url_exists(self):
        self.assertEqual(get_file_or_url('http://algomus.fr/data'), 'data')

    def test_url_and_target_exist(self):
        self.assertEqual(get_file_or_url('http://algomus.fr/data', 'filename'), 'filename')

    def test_url_exists_and_has_params(self):
        self.assertEqual(get_file_or_url('http://algomus.fr/data?param1=value1&param2=value2'), 'value2')

if __name__ == '__main__':
    unittest.main()
