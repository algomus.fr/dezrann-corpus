'''
Handle Dezrann corpora.
'''

import glob
import subprocess
import json
import sys
import os
import os.path
import shlex
import traceback
import datetime
from alive_progress import alive_bar, config_handler
config_handler.set_global(file=sys.stderr)
from string import Formatter
from corpus import KEY, KEYS, VALUE, TEMPLATE, Corpus, ArchiveCorpus, load_corpora
from tools import *
from dezrann_api import *
import archive
import piece

sys.path.append(".")
from metadata import corpora
from rich import print

NOW = datetime.datetime.now()
TMP_JSON = 'tmp.json'
CORPUS_JSON = 'corpus.json'
CORPUS_PATH = 'corpus/'
STATUS = 'doc/status.md'

BUILD = ['template', 'pub', 'down', 'update_meta', 'up', 'corpus', 'restart', 'go']

import argparse

parser = argparse.ArgumentParser(description = 'Prepare/update corpora for the Dezrann server. See add-to-dezrann.txt.')
parser.add_argument('--available', '-a', action='store_true', default=False, help='show available corpora')
parser.add_argument('--all', action='store_true', default=False, help='run on all available corpora (be careful !)')
parser.add_argument('--build', action='store_true', default=False, help='build the corpus, as ' + ' '.join([f'--{a}' for a in BUILD]) + '(be careful !)')
parser.add_argument('--tag',   type=str, help=f'run on tagged corpora [{", ".join(corpora.CORPORA_TAGS.keys())}]')
parser.add_argument('--no-sort', action='store_true', default=False, help='do not sort json keys')

parser.add_argument('--template',  action='store_true', default=False, help=f'process metadata template')
parser.add_argument('--host',   type=str, default=DEFAULT_HOST, help=f'host/server [{", ".join(SERVERS.keys())}], default is "{DEFAULT_HOST}" (be careful !)')
parser.add_argument('--pub',   action='store_true', default=False, help=f'publish to {SERVERS[DEFAULT_HOST]["PUBLISH"]}')
parser.add_argument('--update', action='store_true', default=False, help=f'update already published piece')

parser.add_argument('--overwrite', '--ov',  action='store_true', default=False, help=f'overwrite existing keys')
parser.add_argument('--remove-opus', '--rm-opus',  action='store_true', default=False, help=f"remove all existing keys under 'opus'")
parser.add_argument('--import-opus', action='store_true', default=False, help=f"remove then import keys under 'opus' from existing file")

parser.add_argument('--down', action='store_true', default=False, help=f'download from {SERVERS[DEFAULT_HOST]["RSYNC"]} (require admin access)')
parser.add_argument('--update-meta', action='store_true', default=False, help=f'update local info.json')
parser.add_argument('--up',   action='store_true', default=False, help=f'upload to {SERVERS[DEFAULT_HOST]["RSYNC"]} (require admin access)')

parser.add_argument('--xml',   action='store_true', default=False, help=f'convert to .xml (with musescore)')
parser.add_argument('--mei',   action='store_true', default=False, help=f'convert to .mei (with verovio)')

parser.add_argument('--cleanup-xml',   action='store_true', default=False, help=f'clean up xml with musecore')

parser.add_argument('--corpus', action='store_true', default=False, help="deploy 'corpus.json' and 'access.json'")

parser.add_argument('--go', action='store_true', default=False, help='actually process the data, otherwise dry run')
parser.add_argument('--prod', action='store_true', default=False, help='work on prod data (not implemented for all actions), otherwise on sandbox')
parser.add_argument('--restart', action='store_true', default=False, help='once everything is done, restart Dezrann back')

parser.add_argument('--limit', '-l', type=int, default=None, help='limit to the first n pieces')
parser.add_argument('--filter', '-f', type=str, action='append', default=[], help='filter on the title of the piece (OR between several filters)')

parser.add_argument('--promote', action='store_true', default=False, help="promote the corpus out of sandbox, saving the old one")
parser.add_argument('--archive', action='store_true', default=False, help="prepare an archive")
parser.add_argument('--from-archive', type=str, help="regenerate a corpus from an archive")

parser.add_argument('--status', action='store_true', default=False, help="statistics/overview of corpora")
parser.add_argument('--quiet', '-q', action='count', default=0, help=f'quiet')
parser.add_argument('--verbose', '-v', action='count', default=0, help=f'verbose')

parser.add_argument('corpora', nargs='*', help='''corpora''')




def fix_positions():
    ### TODO: Not used

    # cmd = 'mp3info -p %%S %(key)s/sources/audios/%(key)s/%(key)s.mp3' % {'key': key}
    cmd = f'soxi -D %(key)s/sources/audios/%(key)s/%(key)s.mp3'
    try:
        length = float(exec(cmd))
    except:
        length = None
    print('Length:', length)

    # cmd = f'file {key/sources/audios/%y)s/images/%(key)s-spec/%(key)s-spec.png' % {'key': key}
    # xSize = int(exec(cmd).split()[4])
    print('xSize:', xSize)

    synchro = '%(key)s/sources/audios/%(key)s/synchro.json' % {'key': key}
    if length:
        with UpdateJson(synchro, args.dry) as j:
            j[-1]['date'] = length

        positions = '%(key)s/sources/audios/%(key)s/images/%(key)s-spec/positions.json' % {'key': key}
        with UpdateJson(positions, args.dry) as j:
            j['date-x'][-1] = {'date': length, 'x': xSize}


def try_format(template, keys, template_when_error):
    try:
        # List template
        if is_list_template(template):
            str_template = template[0]
            variables = get_string_template_variables(str_template)
            variable = variables[0]
            if not isinstance(keys[variable], list):
                raise Exception(f"List template '{str_template} require a list variable but '{variable}' is not a list")
            values = keys[variable]
            tf = [str_template.format(**{variable: v}).strip() for v in values]
        # Normal template
        else:
            tf = template.format(**keys).strip()

            # remove any trailing/leading blank or comma
            tf = tf.strip()
            tf = tf.strip(',')
        return tf
    except KeyError as e:
        if template_when_error:
            print(f"! {e} not found for '{template}'")
    except Exception as e:
        print(f"! Template error for '{template}':", e)
    return template if template_when_error else None


def get_string_template_variables(string_template: str) -> list:
    """
    Returns the list of variables in a string template.

    >>> get_string_template_variables("{bli} {bla}")
    ['bli', 'bla']
    """
    return [fn for _, fn, _, _ in Formatter().parse(string_template) if fn is not None]

def is_string_template(entry):
    """Returns true is the entry is a string template."""
    if not isinstance(entry, str):
        return False
    variables = get_string_template_variables(entry)
    return len(variables) > 0


def is_list_template(entry):
    """
    Returns true is the entry is a list template.

    A list template is a list with a single string element, which is a string
    template. This string template must contain one and only one variable, which
    must be a list.

    The principle of a list template is to be deployed through the templating
    process.

    Example:

        If images_ids == [1, 2, 3], then:

            "..sources.1.images": ["https://example.com/{image_ids}.png"]

        will be transformed into:

            "..sources.1.images": ["https://example.com/1.png",
                                   "https://example.com/2.png",
                                   "https://example.com/3.png"]
    """
    if not isinstance(entry, list) or len(entry) != 1 or not is_string_template(entry[0]):
        return False
    str_template = entry[0]
    variables = [fn for _, fn, _, _ in Formatter().parse(str_template) if fn is not None]
    if len(variables) != 1:
        return False
    return True


def update_metadata(f_info, piece_json, metadata, keys, args):

    with open(f_info, 'w') as ff:
        json.dump(piece_json, ff, indent=2)

    with UpdateJson(f_info, args.dry, not args.no_sort, args.verbose) as j:
      for category in ['opus', 'settings']:

        if not category in j or args.remove_opus:
            j[category] = {}

        jo = PointDict(j[category])
                
        if '' in jo.keys():
            # Hack for .., please remove
            del jo['']

        # Transfer existing keys
        for k in jo:
            keys[k.replace(':', '_')] = jo[k]

        # Iterate over KEYS
        for key in keys[KEYS]:
            if args.verbose > 1:
                print('⚙️ ', key)
            if key == 'template':
                if 'template' in metadata:
                    metadata_key = metadata['template'][category] if category in metadata['template'] else {}
            elif not key in metadata['pieces']:
                if args.verbose > 1:
                    print('     SKIP')
                continue
            else:
                if not category in metadata['pieces'][key]:
                    continue
                metadata_key = metadata['pieces'][key][category]

            metadata_key = metadata_key.copy()
            the_keys = list(metadata_key.keys())
            keys_with_new_values = []

            # Iterate in two passes
            for kk, second_iteration in [(k, False) for k in the_keys] + [(k, True) for k in the_keys]:
                if not '..' in kk:
                    # Regular 'opus' value
                    k_, j_ = (kk, jo)
                    existing = k_ in j_ and j_[k_]
                else:
                    k_ = kk.replace('..', '')
                    j_ = j
                    existing = False
                    try:
                        existing = bool(j_[k_])
                        if existing:
                            # Hack, to use templates with '..' keys
                            metadata_key[kk] = j_[k_]
                            existing = False
                    except:
                        existing = False

                if existing and (not k_ in keys_with_new_values or not args.overwrite):
                    # When a previous value was here, leave it as it is, unless --overwrite
                    # When a previous iteration already set that, leave it as it in all cases
                    status = 'skip'
                    val = j_[k_]
                else:
                    keys_with_new_values += [k_]
                    # Compute the value
                    status = ''
                    jj = metadata_key[kk]

                    if (isinstance(jj, list) and not is_list_template(jj)) or kk == 'contributors': 
                        # List (not list template), take it without templating
                        val = jj
                    else:
                        if isinstance(jj, str) or is_list_template(jj):
                            # String or list template
                            template = jj
                        elif isinstance(jj, int):
                            template = str(jj)
                        else:
                            # Dict, indexed by any of the KEYS, possibly through TEMPLATE also
                            template = jj[TEMPLATE] if TEMPLATE in str(jj) else '{%s}' % VALUE
                            for kkk in keys[KEYS]:
                                if kkk in str(jj):
                                    keys[VALUE] = try_format(jj[kkk], keys, second_iteration)
                                    break

                        # Templating
                        val = try_format(template, keys, second_iteration)
                    
                    # Actual store
                    try:
                        j_[k_] = val
                    except TypeError:
                        print("! Bad key:", k_)
                        
                # Store the value for use in next iterations  
                keys[kk.replace(':', '_')] = val
                if args.verbose > 1:
                    print(f' {status:4s} {kk:20s} {val}')

        if category == 'opus' and not 'id' in jo.data.keys():
            jo['id'] = keys[KEY]

        if jo.data:
            j[category] = jo.data
        else:
            del j[category]

    # Reload everything to have pure json data, outside of UpdateJson()
    with open(f_info) as ff:
        j_out = json.load(ff)

    return j_out

def process(f, dir, corpus, args, update_meta=False):

    keys = Corpus.get_keys(corpus, f, args) # TODO: proper method
    if not keys:
        print(f'[red]! {f}: no keys')
        return None

    # Rework metadata with templates
    if args.template:
        f_info = dir + '/' + TMP_JSON
        j = update_metadata(f_info, corpus.pieces[f] if f in corpus.pieces else {}, corpus.d.copy(), keys, args)
        corpus[f] = j

    # Publish piece
    if args.pub or args.update or update_meta or args.archive:
        cwd = os.getcwd()
        os.chdir(dir)
        print(dir)
        try:
            piece.process(corpus, f, args, update_meta)
            os.chdir(cwd)
        except Exception as e:
            os.chdir(cwd)
            raise e

    return True


def filter_and_process(corpus, args, update_meta=False):

    items = corpus.FILES

    if args.filter:
        items = list(filter(lambda i: any([f in i for f in args.filter]), items))
    
    if args.limit is not None:
        items = items[:args.limit]

    print(f"{len(items)} items: {str_list_start(items)}")
    if args.verbose > 0:
        print()

    ok = []
    ko = []

    n = 0
    nn = len(items)

    with alive_bar(nn, file=sys.stderr, enrich_print=False) as bar:
      for f in items:
        bar.text = f
        n += 1
        if args.verbose > 0:
            print(f'[yellow]## {f}[/] ({n}/{nn})')
        if f not in corpus.pieces:
            print(f'[red]! {f}: no metadata')
        try:
            dir = f'corpus/{corpus.ID}/{f}'
            dir = dir.replace(':', '-')
            if args.archive:
               dir = f'{archive.BASE_PATH}/tmp'

            exec(f'mkdir -p "{dir}"', verbose=0)

            status = process(f, dir, corpus, args, update_meta)
            if status == True:
                ok += [f]
        except Exception:
            print(traceback.format_exc())
            ko += [f]
        if args.verbose > 0:
            print()
        bar()

    print()
    if ok:
        print(f'[green]!+ {len(ok)} ok:[/] {str_list_start(ok)}')
    if ko:
        print(f'[red]! {len(ko)} ko:[/] {str_list_start(ko)}')

    if args.template:
        corpus.save()

    if args.corpus:
        try:
            update_corpus_metadata(corpus, prod=args.prod, host=args.host)
            update_corpus_access(corpus, prod=args.prod, host=args.host)
        except:
            print(f'[red]! corpus.json/access.json failed')

    if args.promote:
        try:
            promote(corpus, host=args.host)
        except:
            print(f'[red]! promote failed')

    if args.archive:
        try:
            archive.archive(corpus, args)
        except:
            print(traceback.format_exc())
            print(f'[red]! archive failed')

class Tee(object):
    def __init__(self, *files):
        self.files = files
    def write(self, obj):
        for f in self.files:
            f.write(obj)
    def flush(self):
        for f in self.files:
            try:
                f.flush()
            except:
                pass

if __name__ == '__main__':

    start_time = datetime.datetime.now().replace(microsecond=0)
    args = parser.parse_args()

    if args.build:
        a = vars(args)
        for arg in BUILD:
            a[arg.replace('-', '_')] = True

    args.dry = not args.go

    print('[magenta]!', start_time)
    print('[yellow]!', ' '.join(sys.argv))

    CORPORA_LIST = []

    if args.from_archive:
        CORPORA_LIST += [ArchiveCorpus(args.from_archive)]
    else:
        CORPORA_LIST += corpora.AVAILABLE_CORPORA

    CORPORA = load_corpora(CORPORA_LIST, args)

    if args.available or (not args.corpora and not args.all):
        for corpus in CORPORA:
            print(corpus.ID)

    if args.status:
        print('[yellow]<==', STATUS)
        status = open(STATUS, 'a')
        
    if args.tag:
        name, c = corpora.CORPORA_TAGS[args.tag]
        if args.status:
            status.write(f'\n\n## {name}\n\n')
        args.corpora = c

    if args.all or args.from_archive:
        args.corpora = [c.ID for c in CORPORA]
    if args.all:
        args.quiet += 1

    args.verbose += 1 - args.quiet
    stdout = sys.stdout

    status_table = {}
    status_str = ''

    for c in args.corpora:
      start_time_c = datetime.datetime.now().replace(microsecond=0)
      log = f'{c}.log'
      print(f'<== {log}')
      with open(log, 'a') as f:
        sys.stdout = Tee(stdout, f)
        print(f'\n\n\n[green]#### {NOW}\n')
        print(f'[cyan]### {c}')
        try:
            corpus = { c.ID: c for c in CORPORA }[c]
        except KeyError:
            print("## Corpus '%s' not found" % c)
            continue


        filter_and_process(corpus, args) 
        print()

        if args.down:
            download(c, prod=args.prod, dry=args.dry, host=args.host)

        if args.update_meta:
            print("### Updating metadata")
            filter_and_process(corpus, args, update_meta = True) 

        if args.up:
            upload(c, prod=args.prod, dry=args.dry, host=args.host)

        if args.status:
            status_row, str = corpus.status()
            status_table[c] = status_row
            status_str += str

        elapsed_time_c = datetime.datetime.now().replace(microsecond=0) - start_time_c
        print(f"[magenta]!## Elapsed time: {elapsed_time_c}")
        print()

        sys.stdout = stdout

    if args.status:
        COLS = [
            ('ID_', ''),
            # 'title',
            ('quality:corpus_', 'C'),
            ('quality:corpus:metadata_+quality:metadata_', 'M'),
            ('build_', 'build'),
            ('pieces_+score_on_server', 'pcs_'),
            # 'sources_',
            ('score_+quality:score_', '🎶'),
            ('mmap_+quality:musical-time_', '🕒'),
            ('analysis_+quality:annotation_', '🏷️'),
            ('audio_+quality:audio_+audio_on_server', '🔊'),
            ('video_', '▶️'),
            ('synchro_+quality:audio:synchro_', 's'),
            # 'quality_',
            ('license_', 'L'),
            ('issues_', 'I'),
        ]
        print(status_table)
        status.write(f'''<div style='font-size:66%'>{format_table_md(status_table, COLS)} </div>''')
        status.write(status_str)

    if args.restart:
        restart_back(args.dry, host=args.host)

    elapsed_time = datetime.datetime.now().replace(microsecond=0) - start_time
    print(f"[magenta]!## Total elapsed time: {elapsed_time}")