# Simple colored output

import subprocess
import os
import json

WGET = 'wget --no-check-certificate'

CSIm = '\033[%sm'

class ANSI:
    RESET = 0
    BRIGHT = 1
    BLACK = 30
    RED = 31
    GREEN = 32
    YELLOW = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    WHITE = 37

def color(col, text, colorize = True):
    if not colorize:
        return text
    return CSIm % col + text + CSIm % ANSI.RESET

def roman(i):
    ROMANS = [ '0', 'i', 'ii', 'iii', 'iv', 'v', 'vi', 'vii', 'viii', 'ix', 'x']
    if i < len(ROMANS):
        return ROMANS[i]
    return str(i)

def pluralize(s, n):
    return s + 's' if n != 1 else s

def str_stats_distribution(d, short=False):
    if not len(d):
        return '-'

    s = 0
    n = 0
    try:
        for val, nb in d.items():
            s += int(val) * nb
            n += nb
        avg = f'{s/n:.1f}'
    except:
        print('!', d)
        avg = '?'

    if short:
        return f'{avg}'

    avg = render_quality(avg)

    if len(d) == 1:
        return f'{avg} [{n}]'

    try:
        out = f'{render_quality(min(d.keys()))}→{render_quality(max(d.keys()))} (avg {avg}) [{n}]'
    except:
        out = '??'

    return out

def exec(cmd, check=True, dry=False, verbose=1):
    if verbose > 0:
        print(color(ANSI.CYAN, cmd))
    if dry:
        return 'dry run'
    p = subprocess.run(cmd, check=False, capture_output=True, text=True, shell=True)
    if p.stderr:
        print(color(ANSI.RED if p.returncode else ANSI.MAGENTA, p.stderr))
    if p.stdout:
        print(p.stdout)
    if check:
        p.check_returncode()
    return p.stdout


def get_file_or_url(f, target=None, path='', dry=False):
    if f.startswith('http') or f.startswith('file'):
        if not target:
            target = os.path.basename(f)
            if '&' in target and '=' in target:
                i = target.rindex('=')
                target=target[i+1:]
            if path:
                target= path + '/' + target

    if f.startswith('http'):
        try:
            if dry:
                exec(f'touch {target}')
            else:
                exec(f'{WGET} -nv "{f}" -O "{target}"')
        except:
            print(color(ANSI.RED, f'! {f}'))
            return None
        return target
    if f.startswith('file:'):
        f = f.replace('file:', '')
    if path:
        f = path + '/' + f
    if not target:
        if not os.path.exists(f):
            print(color(ANSI.RED, f'! {f}'))
            return None
        return f
    else:
        try:
            exec(f'cp -pr "{f}" "{target}"')
        except:
            print(color(ANSI.RED, f'! {f}'))
            return None
        return target

def str_list_start(l, max_size=140):
    s = str(l)
    if len(s) > max_size:
        s = s[:max_size - 4] + '...]'
    return s

from functools import reduce

def format_table_md(table, keykeys=None):
    if keykeys:
        keys = []
        kkeys = []
        for k, kk in keykeys:
            keys += [k]
            kkeys += [ f"<span title='{k.replace('+', ', ')}'>{kk}</span>"]
    else:
        kk = [row.keys() for row in table.values()]
        keys = reduce(lambda x, y: x.union(y), kk)
        kkeys = keys

    def get_vals(row, k):
        'Handles combined keys, with "+"'
        return ' '.join([str(row[kk]) for kk in k.split('+')])

    s = '\n\n'
    s += '| ' + ' | '.join(k.replace('_', ' ') for k in kkeys) + ' |\n'
    s += '| ' + ' | '.join('--' for k in keys) + ' |\n'
    for row in table.values():
        s += '| ' + ' | '.join(get_vals(row, k) for k in keys) + ' |\n'
    s += '\n'
    return s

def render_quality(q):
    try:
        qq = int(float(q))
    except:
        qq = 'x'
    return f'<span class="q{qq}">{q}</span>'

from collections import UserDict

class MetaDict(UserDict):

    EMPTY = '⚠️ Empty value'

    FALLBACKS = {
        'shorttitle': ['title']
    }

    def __getitem__(self, key):
        '''
        Check for
           xxx:en, then xxx, then same things with ':' instead of '_',
           then FALLBACKS, otherwise return EMPTY
        '''

        fallbacks = self.FALLBACKS[key] if key in self.FALLBACKS else []

        for k in [key, key.replace('_', ':')] + fallbacks:
            if f'{k}:en' in self:
                return super().__getitem__(f'{k}:en')
            if k in self:
                return super().__getitem__(k)
        return self.EMPTY

class PointDict(UserDict):
    '''
    >>> d = PointDict()
    >>> d['foo.bar'] = 1
    >>> d['boo'] = [2, 3, {}]
    >>> d['boo.1'] = 5
    >>> d['boo.2.bla'] = 'zaa'
    >>> d['zaa.1'] = 4
    >>> d
    {'foo': {'bar': 1}, 'boo': [2, 5, {'bla': 'zaa'}], 'zaa': [{}, 4]}

    >>> d['foo']
    {'bar': 1}
    >>> d['boo.0']
    2
    >>> d['foo.bar']
    1
    >>> d['boo.2.bla']
    'zaa'

    >>> d['boo.3']
    Traceback (most recent call last):
     ...
    IndexError: list index out of range
    >>> d['foo.zoo']
    Traceback (most recent call last):
     ...
    KeyError: 'zoo'
    '''


    def dive(self, keys, create):
        l = keys.split('.')
        d = self
        dd = d
        for i, key in enumerate(l):
            if isinstance (d, list) and key.isdigit():
                key = int(key)
                if create:
                    while len(d) <= key:
                        d += [{}]
            if not isinstance(d, list) and not key in d:
                if create:
                    if i < len(l)-1 and l[i+1].isdigit():
                        d[key] = []
                    else:
                        d[key] = {}
                else:
                    raise KeyError(key)
            dd = d
            d = d[key]

        return dd, key

    def __setitem__(self, keys, value):
        if '.' not in keys:
            return super().__setitem__(keys, value)
        d, k = self.dive(keys, True)
        d[k] = value

    def __getitem__(self, keys):
        if '.' not in keys:
            return super().__getitem__(keys)
        d, k = self.dive(keys, False)
        return d[k]

    def __str__(self):
        return 'PointDict %s' % super().__str__()


# https://stackoverflow.com/a/39234239
def walk_items(data, path=None, parent=None, key=None):
    '''
    Walk through the items of a PointDict-like object ,
    yielding (path, parent, key, value), with
    data[path] = parent[key] = value.

    `parent[key] = value` can then be used to update in-place
    the original object.
    '''
    if path is None:
        path = []
    if type(data) is dict:
        for key, value in data.items():
            yield from walk_items(value, path + [key], data, key)
    elif type(data) is list:
        for key, value in enumerate(data):
            yield from walk_items(value, path + [str(key)], data, key)
    else:
        yield ('.'.join(path), parent, key, data)


class UpdateJson():
    '''Create and/or Update a .json file'''
    def __init__(self, f, dry=False, sort=True, verbose=0, overwrite=False):
        self.dry = dry
        self.sort = sort
        self.f = f
        self.out = f + '.dry' if dry else self.f
        self.verbose = verbose
        self.overwrite = overwrite

    def __enter__(self):
        # Create file when it does not exists or if overwrite
        if not os.path.exists(self.f) or self.overwrite:
            with open(self.f, 'w') as ff:
                json.dump({}, ff)

        # Read file
        if self.verbose > 0:
            print('<==', self.f)
        with open(self.f) as ff:
            self.j = PointDict(json.load(ff))
            return self.j

    def __exit__(self, type, value, traceback):
        if self.verbose > 0:
            print('==>', self.out)
        with open(self.out, 'w') as ff:
            json.dump(self.j.data, ff, indent=2, sort_keys=self.sort)
        if self.dry:
            os.system('diff --color=auto -u "%s" "%s"' % (self.f, self.out))

