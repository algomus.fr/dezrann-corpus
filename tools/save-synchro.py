from tools import exec
from rich import print
import glob
import re


# Remote server
SERVER = 'alb:corpus/'

# Local directories
DIR_TMP = 'save-synchro-tmp'
DIR_CORPUS = 'corpus/'

# Order of arguments is important
RSYNC_ONLY_SYNCHRO = 'rsync -zar --info=stats1 --include="*/" --include="synchro.json" --exclude="*" -m' 

SED_SIMPLIFY_DATE = r"""sed -i 's/\("date": .*[.][0-9][0-9]\)[0-9]*/\1/' """

# List of corpora and tags
TAGS_GENERIC = [
            '-0-yt',
            '-1-yt',
            '-2-yt',
            '-3-yt',
]

CORPORA = [    
  ('collabscore', 'collabscore', { }),
  ('bach-fugues', 'bach-fugues',
    {
        '-0-yt': 'AllOfBach',
        '-0': 'Ishizaka',
        '-1-yt': 'Ishizaka-yt',
    }),
  ('mozart-piano-sonatas',   'mozart-piano-sonatas', {'-0-yt': 'Würtz-yt' }),
  ('mozart-string-quartets',  'mozart-string-quartets', {'-0': 'BSQ' }),
  ('classical-symphonies/haydn-symphonies', 'classical-symphonies', {'-0': 'RPO' }),
  ('classical-symphonies/mozart-symphonies', 'classical-symphonies', {'-0': 'Bamberger' }),
  ('classical-symphonies/beethoven-symphonies', 'classical-symphonies', {'-0': 'RPO' }),
  ('openscore-lieder', 'openscore-lieder', {'-0-yt': 'KolbShrut1992' })
]

RE_PIECE_AUDIO = re.compile('.*/(?P<piece>[^/]*)/sources/audios/(?P<audio>.*)/synchro.json')

RE_SAVED_SYNCHRO = re.compile(f'.*/(?P<corpus>[^/]*)/synchro/(?P<piece>[^/]*)--(?P<tag>.*).synchro.json')

import argparse

parser = argparse.ArgumentParser(description = 'Download/upload synchro files from/to the Dezrann server.')

parser.add_argument('--down', action='store_true', default=False, help=f'download from {SERVER} (require admin access)')
parser.add_argument('--up',   action='store_true', default=False, help=f'upload to {SERVER} (require admin access)')
parser.add_argument('--verbose', '-v', action='count', default=0, help=f'verbose')

args = parser.parse_args()

def download(corpus, dest_tmp, dest_corpus, tags):
    exec(f'{RSYNC_ONLY_SYNCHRO} {SERVER}/{corpus} {dest_tmp}')  
    print(f'{dest_tmp} [green]==>[/] {dest_corpus}')
    exec(f'rm -f ; mkdir -p {dest_corpus}')
    nb = 0
    other = 0

    for f in glob.glob(f'{dest_tmp}/*/*/sources/audios/*/synchro.json'):
        if args.verbose:
            print(f)
        m = RE_PIECE_AUDIO.match(f)
        if not m:
            print('!', f)
            continue
        
        id_audio = m['audio'].replace(m['piece'], '')

        ## Hack for id
        mp = m['piece'] # .replace('all-collabscore-saintsaens-ref-', '')
        id_audio = id_audio.replace(mp, '')
        
        if not id_audio in tags.keys():
            print('!', f, id_audio, tags)
            other += 1
            if id_audio in TAGS_GENERIC:
                tag = id_audio
            else:
                tag = f'other-{other}'            
        else:
            tag = tags[id_audio]

        f_dest = f"{dest_corpus}/{m['piece']}-{tag}.synchro.json"
        
        print(f_dest)
        exec(f'cp -pr {f} {f_dest}', verbose=0)
        exec(f'{SED_SIMPLIFY_DATE} {f_dest}')
        nb += 1
           
    print(f'== {corpus} ==> {nb} synchro files ({other} without tags)') 
    print()

def upload(corpus, dest_tmp, dest_corpus, tags):
    print(f'{dest_tmp} [yellow]<==[/] {dest_corpus}')
    # exec(f'{RSYNC_ONLY_SYNCHRO} {SERVER}/{corpus} {dest_tmp}')

    for f in glob.glob(f'{dest_corpus}/*synchro.json'):
        m = RE_SAVED_SYNCHRO.match(f)
        if not m:
            print('!', f)
            continue
        print(f)

        ## Hack for id
        audio_id = m['piece'] # .replace('all-collabscore-saintsaens-ref-', '')
        audio_tag = m['tag'] # TODO: reverse lookup

        ff = f"{dest_tmp}/{m['corpus']}/{m['piece']}/sources/audios/{audio_id}-{audio_tag}/synchro.json"
        print(ff)
        try:
            exec(f'cp -pr {f} {ff}', verbose=1)
        except:
            print('[red]! Upload synchro failed')

    exec(f'{RSYNC_ONLY_SYNCHRO} {dest_tmp}/{corpus} {SERVER}')

if __name__ == '__main__':

    print()

    if args.down:
        exec(f'rm -rf {DIR_TMP} ; mkdir {DIR_TMP}')

    for (corpus, target, tags) in CORPORA:
        dest_tmp = f'{DIR_TMP}/{target}'
        dest_corpus = f'{DIR_CORPUS}/{target}/synchro'
        print(f'[yellow]## {corpus} <=> {target}[/yellow]')

        if args.down:
            download(corpus, dest_tmp, dest_corpus, tags)

        if args.up:
          if corpus == 'collabscore':
            upload(corpus, dest_tmp, dest_corpus, tags)
