
from tools import *
import json
from rich import print
import shlex
import os.path
from hosts import *

HOST = DEFAULT_HOST
# HOST = 'alb'

NEUMA = f'''curl -sS --request POST \
  --url %s/?neuma_id=%s' \
  --header 'Content-Type: multipart/form-data'
'''

SERVER_PUBLISH_ARG = 'score'
# SERVER_PUBLISH = 'http://alb.algomus.net/publish/'
# SERVER_PUBLISH_ARG = 'mei'

ACCESS_JSON = 'access.json'
ACCESS_JSON_EMPTY = {
  "read": [],
  "read-analysis": [],
  "update-analysis": [],
  "update-synchro": []
}

CORPUS_PATH = 'corpus/'
CORPUS_JSON = 'corpus.json'

def get_server_rsync(prod, host=HOST):
    return SERVERS[host]['RSYNC_PROD'] if prod else SERVERS[host]['RSYNC']

RSYNC_OPTS = ''' --include="*/"  --include "*.json" --include "*.dez"  --include "*.wav" --exclude="*" '''
RSYNC_DRY = ' --dry-run '

def info(corpus, host=HOST, prod=False, dry=False):
    server = get_server_rsync(prod, host)
    serv = server[:server.index(':')]
    
    com = f'''ssh {serv} "date +%F -r {CORPUS_PATH}/{corpus} ; du -sh {CORPUS_PATH}/{corpus}"''' 
    out = exec(com, dry).replace('\n', ' ').split()
    return ' '.join(out[:2])

def download(corpus, host=HOST, prod=False, dry=False):
    server = get_server_rsync(prod, host)
    com = f'''rsync -avm {server}/{corpus} {RSYNC_OPTS} {CORPUS_PATH} '''
    if dry:
        com += RSYNC_DRY
    # subprocess here does not work...
    print(color(ANSI.MAGENTA, com))
    os.system(com)  

def upload(corpus, host=HOST, prod=False, dry=False):
    server = get_server_rsync(prod, host)
    com = f'''rsync -avm {CORPUS_PATH}/{corpus}/ {RSYNC_OPTS} {server}/{corpus}/ '''
    print(color(ANSI.MAGENTA, com))
    os.system(com)

def xml_cleanup(xml_file_name, dry):
    com = f'musescore.mscore {shlex.quote(xml_file_name)} -o {shlex.quote(xml_file_name)}'
    exec(com, dry=dry, check=False)

def mscore_to_xml(f, f_base, dry):
    f_tmp = f'{f_base}-tmp.xml'
    ff = f'{f_base}.xml' # ff could be equal as f
    com = f'musescore.mscore {shlex.quote(f)} -o {shlex.quote(f_tmp)}'
    exec(com, dry=dry, check=False)
    com = f'mv -v {shlex.quote(f_tmp)} {shlex.quote(ff)}'
    exec(com, dry=dry, check=False)
    return ff

def create_mmap(f, dry):
    com = f'MM extract -d . -r {f}'
    exec(com, dry=dry, check=False)
    ff = os.path.splitext(f)[0] + '.mm.json'
    return ff
    
def to_mei(f, f_base, dry):
    f_mei = f'{f_base}.mei'
    VEROVIO_OPTIONS = '--breaks none --bar-line-width 0.35 --font Bravura --header none --spacing-linear 0.35'
    com = f'verovio -a -t mei {VEROVIO_OPTIONS} {shlex.quote(f)}'
    exec(com, dry=dry, check=False)
    com = 'sed -i "s/<title..>/<title>Bla<\/title>/" '+ shlex.quote(f_mei)
    exec(com, dry=dry, check=False)
    return f_mei


FFMPEG_CONVERT = 'ffmpeg  -v quiet -stats -i %s -vn -ar 44100 -ac 2 -b:a 192k %s'

def to_mp3(f):
    ff =  os.path.splitext(f)[0] + '.mp3'
    if f == ff:
        return f
    if f.endswith('.mp3'):
        return(f)
    exec(f'rm -f {ff}')
    com = FFMPEG_CONVERT % (f, ff)
    exec(com)
    return ff


def publish(path, dry,
            f_score=None,
            f_audio=[],
            f_info=None,
            host=HOST):
    com = f'curl --silent '
    if f_score:
        com += f'-F {SERVER_PUBLISH_ARG}="@{f_score}" '
    for file_to_upload in f_audio:
        com += f'-F audio="@{file_to_upload}" '
    if f_info:
        com += f'-F infos="@{f_info}" '
    com += f'{SERVERS[host]["PUBLISH"]}/{path}'
    out = exec(com, dry=dry)
    if 'Aborted(OOM)' in out:
        return False
    return True

def publish_image(piece, image, positions, dry=False, prod=False, host=HOST):
    server = get_server_rsync(prod, host)
    server_static = SERVERS[host]['RSYNC_STATIC']
    serv = server[:server.index(':')]
    print("XXX", image, positions)
    root = server.split(':')[1]
    root_static = server_static.split(':')[1]
    image_name = image.split('.')[0]
    scan_dir = f'''{piece}/sources/images/scan/'''
    positions_dir = f'''{piece}/sources/images/{image_name}/'''
    com = f'''
        ssh {serv} "mkdir -p {root}/{scan_dir}";
        ssh {serv} "mkdir -p {root}/{positions_dir}";
        ssh {serv} "mkdir -p {root_static}{scan_dir}";
        scp -pr {image} {server_static}{scan_dir};
        scp -pr {positions} {server}/{positions_dir}/positions.json;
    '''
    out = exec(com, dry=dry)

def publish_analyses(piece, analyses, dry=False, prod=False, host=HOST):
    server = get_server_rsync(prod, host)
    for f in analyses:
        com = f'''scp -pr {f} {server}/{piece}/analyses/ '''
        out = exec(com, dry=dry)


def publish_static(corpuspiece, f, dry=False, prod=False, host=HOST):
    server = get_server_rsync(prod, host)

    # Get directory, removing also server name
    full = f'{server}/{corpuspiece}'
    dir = full[full.index(':')+1:]
    serv = server[:server.index(':')]

    # Mkdir then scp
    com = f'''ssh {serv} "mkdir -p {dir}"'''
    out = exec(com, dry=dry)
    com = f'''scp -pr {f} {server}/{corpuspiece}/'''
    out = exec(com, dry=dry)

    # Return api path
    fb = os.path.basename(f)
    print('==>', fb)
    return fb

def promote(corpus, dry=False, host=HOST):
    try:
        exec(SERVERS[host]['SAVE_OLD'].format(ID = corpus.ID), dry=dry)
    except:
        print('[red]! No backup done, probably there was not an old corpus there')
    exec(SERVERS[host]['PROMOTE'].format(ID = corpus.ID), dry=dry)

def update_synchro(path, pieceid, index, synchro, dry=False, prod=False, host=HOST):
    if not synchro:
        return
    server = get_server_rsync(prod, host)
    com = f'''scp -pr {synchro} {server}/{path}/sources/audios/{pieceid}-{index}/synchro.json '''
    out = exec(com, dry=dry)

def update_corpus_metadata(corpus, dry=False, prod=False, host=HOST):
    corpus_json = CORPUS_PATH + '/' + corpus.ID + '/' + CORPUS_JSON
    print(f'[yellow]==> {corpus_json}')
    with open(corpus_json, 'w') as f:
        json.dump({'corpus': corpus.d['corpus']}, f, indent=2)
    server = get_server_rsync(prod, host)
    com = f'''scp -pr {corpus_json} {server}/{corpus.ID}/ '''
    out = exec(com, dry=dry)

def update_corpus_access(corpus, dry=False, prod=False, host=HOST):
    who = corpus.settings('access')
    if not who:
        return
    access_json = CORPUS_PATH + '/' + corpus.ID + '/' + ACCESS_JSON
    access = ACCESS_JSON_EMPTY.copy()
    for k in access.keys():
        access[k] += [who]
    with open(access_json, 'w') as f:
        json.dump(access, f, indent=2)
    server = get_server_rsync(prod, host)
    com = f'''scp -pr {access_json} {server}/{corpus.ID}/ '''
    out = exec(com, dry=dry)

def restart_back(dry=False, host=HOST):
    print(color(ANSI.YELLOW, '# restarting Dezrann back'))
    exec(SERVERS[host]['RESTART'], dry=dry)


def generate_from_neuma(neuma_id, dry=False):
    cmd = NEUMA % (SERVERS[host]["PUBLISH"], neuma_id)
    exec(cmd, dry=dry)
    
