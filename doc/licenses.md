# Open Science and Licenses

Open science requires open licenses to align with FAIR principles. Open licenses ensure that data is Findable, Accessible, Interoperable, and Reusable, fostering transparency, reproducibility, and collaboration in research. These licenses provide the necessary legal framework to share and reuse resources responsibly, reducing barriers to entry for researchers, educators, and data users.

In the context of Dezrann, open licenses are essential for both the datasets it hosts and the code powering the platform. While the FAIR principles primarily apply to data, open-source licensing for code ensures transparency, adaptability, and community-driven development. By using open licenses, Dezrann enables researchers to contribute and access datasets with clear permissions, fostering collaboration and facilitating innovative musicological and computational research.

## License for the Dezrann platform

Dezrann is open-source, licensed under the **GPL version 3 or any later version**.

## Licenses from corpus data

Licenses are often a complex subject when it comes to corpus data. Different components of a corpus—such as scores, audio, and annotations—may come with varying licenses, each with its own set of permissions and restrictions. 
This can create challenges in ensuring that the entire corpus is both legally compliant and usable under open science principles.

Our responsibility is to ensure that the data we use comes from sources with a clear license statement. 
However, we cannot be held accountable for actions taken by upstream providers prior to our use. When there are different licenses, we prioritize the "most open" one—for example, if a part of a dataset has been released under a more permissive license.

It is crucial to maintain a record of these licenses. Years later, the original upstream repository may have disappeared or been altered, but the original license remains valid. Almost all open-data licenses are not time-limited. 
While we are not legal experts, we will comply with requirements from legitimate content owners if such concerns are raised.

As a "good faith" practice, we store one (or several, in cases of multiple sources) screenshot(s) of a page, paper, or archive where the license is displayed. These screenshots should clearly show the source (URL, paper, etc.) and be saved in the [`metadata/licenses`](https://gitlab.com/algomus.fr/dezrann/dezrann-corpus/-/tree/main/metadata/licenses) folder on the `dezrann-corpus` git.

## Non-free data

The Dezrann platform may include non-free data when necessary. For example, non-free audio or video content can be accessed through the YouTube player. Dezrann complies with YouTube's regulations, ensuring that audio and video content is streamed directly from YouTube's servers.
