
# Describing quality of a corpus

Research in Computational Music Analysis and related fields rely on high-quality musical corpora to model music concepts and develop and evaluate computational models, algorithms, and methodologies. Adhering to the FAIR principles, the community aims to share these corpora in a manner that is maximally reproducible.

However, the availability of musical data from various sources introduces inherent variations in quality, making it crucial to establish criteria for assessing and describing the quality of these corpora across multiple dimensions. These dimensions include the completeness and accuracy of scores, audio/video or other accompanying documents, synchronization between different media types, availability and quality of annotations, as well as piece-level and corpus-level metadata.

Through the evaluation and documentation of the quality of musical corpora, researchers can effectively navigate the complexities associated with working with heterogeneous data sources. Furthermore, this process facilitates the promotion of reliable and well-constructed corpora, ensuring their optimal utilization in the field of computational music analysis.

Below, each criterion is assigned a score ranging from 0 to 5, indicating the level of quality and suitability for publication and research purposes:

- 0, 1, 2: we do not want to publish that (or maybe with very strong warnings), bad PR 
- 3, 4, 5: ✔️ we may publish that

Quality ratings of 3 and 4 are already (very) good. A quality rating of 5 should be reserved for very good or exceptional materials, particularly those that are fully reproducible, open, and published.

This document was created as part of the data preparation process for the Dezrann platform. Some of the criteria below, indicated by italics, are specifically relevant to the integration of corpora into Dezrann. Evaluating these quality criteria for the Dezrann corpora indicates that the sources, annotations, and metadata are available in formats compatible with Dezrann.

However, these quality assessments can be applied in a broader context beyond the Dezrann platform. We welcome inputs from other teams or platforms on the criteria for evaluating quality elements. This collaborative approach ensures that we consider diverse perspectives and factors relevant to assessing the quality of musical corpora.

## Example of .json file with quality information

We describe quality criteria for the corpus, as a whole, and for each piece.

```json
{
    "corpus": {
        "title": "Fanny Mendelssohn Songs",
        (...)
        "quality:corpus": "2",
        "quality:corpus:metadata": "3"
    },

    "pieces": {
        "gondelied": {
            "opus": {
                "title": "Gondelied"
                (...)
            },

            "quality:annotation": "2",
            "quality:audio:synchro": "3",
            "quality:audio": "1",
            "quality:metadata": "3",
            "quality:musical-time": "3",
            "quality:score": "4",

            "sources": [
                (...)
            ]
        }
    }
}
```

As of 2024 Q4, we store all piece-related quality information *directly in the piece dictionary*, even when fields relate to a particular source (such as `quality:audio`).


## Quality criteria for the corpus

### quality:corpus
``Sources'' can be scores, audio/video with good synchronization, images, or an annotation set.

-  0: individual pieces, no real collection, no `corpus.json`
-  1: work has started, pieces just landed in Dezrann, first `corpus.json`
-  2: work in progress, should be a nice corpus soon, however for many pieces there are still problems
-  3: ✔️ maintainer identified, at least two "quality sources" (with quality >= 3) for most pieces
-  4: ✔️ maintainer identified, at least three "quality sources" for most pieces, published/referenced corpus in an article/database/git, *documented steps to build/publish the corpus on Dezrann*
-  5: ✔️ maintainer identified, at least three "quality sources" for most pieces, published/referenced corpus in an article/database/git, *fully reproducible steps to build/publish the corpus on Dezrann starting from a long-term archive*

### quality:corpus:metadata
See [metadata.md#corpus-metadata](./metadata.md#corpus-metadata).

-  0: no corpus metadata
-  1: draft corpus metadata
-  2: draft corpus metadata, with a draft description and availability/summary
-  3: ✔️ good corpus metadata (including description, with references and links, and availability/summary of the available data)
-  4: ✔️ same things, with also translation of titles/description/availability in at least 5 languages, and most pieces with `quality:metadata` of 3 or above
-  5: ✔️ same things, with also translation of titles/description/availability in at least 10 languages, and most pieces with `quality:metadata` of 4 or above

## Quality criteria for each piece in the corpus

### quality:score
-  0: no score *(or score not working through the Dezrann pipeline)*
-  1: draft score, for example automatically produced from MIDI, many bugs, not coherent at the end
-  3: ✔️ useable score (even if not beautiful or some strange things in some measures)
-  4: ✔️ near production-quality score (dynamics, typesetting, editing...) (as for OpenScore)
-  5: ✔️ production-quality score, linked to some published critical edition

### quality:musical-time
The musical time (time signatures, measure numbers, repeat structure) should be indicated through a [measure map](https://github.com/MarkGotham/bar-measure) (current discussions with M. Gotham and J. Hentschel).

-  0: no musical time (only audio)
-  1: some musical time, but not coherent everywhere, changes of meter or other things are not well described
-  3: ✔️ useable musical time / measure numbers
-  4: ✔️ useable musical time / measure numbers, with a measure map,
-  5: ✔️ perfect musical time / measure numbers, with a measure map, linked to some published edition, published in a git

### quality:audio:synchro
-  0: no audio
-  1: no synchronisation between audio and musical time (apart from start/end)
-  2: some synchronisation, but sometimes very far away
-  3: ✔️ useable synchronisation, including tempo changes (at the measure level))
-  4: ✔️ good synchronisation (including repeats, at the measure level)
-  5: ✔️ good synchronisation (including repeats, at the beat level), also published in a git

As of 2025, the editor to synchronize repeats is still prototype, so the maximal score is here 3 when there are repeats.

### quality:audio

-  0: no audio
-  1: (horrible) generated audio
-  3: ✔️ generated audio with better quality (instrument, dynamics, expression...) or low-value recorded audio
-  4: ✔️ mid-/high-value recorded audio
-  5: ✔️ high-value recorded audio, 'professional quality' and/or musicological significance, with open licenses

Low/high-value is subjective: It is linked to *audio quality*, but even more on *interpretation quality* and/or *musicological/historical value*: a noisy "Gershwin by Gershwin" is probably more valuable than a 24-bit 96kHz recording from a piano beginner.

Audio (and video) content is particular, as some materials are only accessible via external sites, such as the YouTube player. Licenses are described elsewhere (`licence` for each source). However, content with a `quality:audio` of 5 must be available with an open license.

### quality:annotation
-  0: no annotation
-  1a: draft annotations, musical work need to be done
-  1b: annotations from another source, but technical work need to be done for integration
-  2: quality annotations, research/musical/technical work is still underway
-  3: ✔️ good quality annotations (both musical and technical integration)
-  4: ✔️ good quality annotations (both musical and technical integration), also published in a git
-  5: ✔️ good quality annotations (both musical and technical integration), also published in a git + paper

### quality:metadata
See [metadata.md#opus-piece-metadata](./metadata.md#opus-piece-metadata)

-  0: no piece metadata
-  1: draft metadata, automatically produced, not checked on that piece
-  3: ✔️ good piece metadata
-  4: ✔️ good piece metadata, reviewed/curated on that piece and coherent/homogeneous with the other pieces in the corpus, with significant source identification (`ref:*` fields, in particular `ref:rism` or `ref:musicbrainz`)
-  5: ✔️ same things, also describing piece quality with all relevant fields described here
