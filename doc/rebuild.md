
# Rebuilding Dezrann public corpora

Corpora are described through .json files (see [metadata](./metadata)).
As of Q1 2025, most corpora are built on the Dezrann host from these files through the `tools/dezrann-corpus.py` script located in the [dezrann-corpus](https://gitlab.com/algomus.fr/dezrann/dezrann-corpus) repository.
Several corpora also require preliminary steps to prepare data and/or metadata from upstream archives.

The `--build` option of `tools/dezrann-corpus.py` is a shorthand for:

- `--template`: when applicable (`template=True` in `corpora.py`), process locally metadata templates
- `--pub`: publish the piece on the Dezrann host
- `--down --update-meta --up`: after publication of all pieces, get the metadata (`info.json`) from the host, update them, and upload them back. This is needed to display properly source metadata
- `--corpus`: once the corpus was built, upload also corpus metadata (`corpus.json`) and grant accesses (`access.json`)
- `--restart`: restart the host to take into account the new coprus
- `--go`: actual run of all the above command (otherwise dry run)

## Prerequisites

To rebuild these corpora, you need an account with admin access on a Dezrann host, by either:

- contacting us for a account on our internal Dezrann test server;

- or (⚠️ documentation in progress) following [these instructions](https://gitlab.com/algomus.fr/dezrann/dezrann-front/-/blob/dev/docs/en/install.md) to install your own Dezrann frontend and backend.
Then edit `tools/hosts.py` to add your (backend) host,
and change the `DEFAULT_HOST` to use your host.

You can also rebuild corpora on a specific host using `--host myhost` on any `dezrann-corpus` command.

## Corpus

### bach-fugues » The Well-Tempered Clavier, Book I

From `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build bach-fugues
```

### mozart-piano-sonatas » Mozart Piano Sonatas

From `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build mozart-piano-sonatas
```

Alternative option, after downloading long-term archive from <https://doi.org/10.57745/OHRWPC>:
```sh
python tools/dezrann-corpus.py --build --from-archive mozart-piano-sonatas.zip
```

### mozart-string-quartets » Mozart String Quartets

From `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build mozart-string-quartets
```

### classical-symphonies » Classical and Early-Romantic Symphonies

From `dezrann-corpus`:  
```sh
python tools/dezrann-corpus.py --build classical-symphonies
python tools/dezrann-corpus.py --build mozart-symphonies
python tools/dezrann-corpus.py --build haydn-symphonies
python tools/dezrann-corpus.py --build beethoven-symphonies
```

⚠️ As the scores are very large, and as the audio files are quite long, this build demands large quantities of memory on the Dezrann backend.

Then, on the server (the corpus actually contains three subcorpora, we need to automatize that):

```
mv sandbox/classical-symphonies/ .
mv sandbox/beethoven-symphonies/ classical-symphonies/
mv sandbox/mozart-symphonies/ classical-symphonies/
mv sandbox/haydn-symphonies/ classical-symphonies/
```


### schubert-winterreise » Winterreise (Winter Journey)

Follow instructions at <https://gitlab.com/algomus.fr/dezrann/schubert-winterreise> to prepare all data.

Clone this repository as a sibling directory of `dezrann-corpus`

Then, from `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build schubert-winterreise
```

### openscore-lieder » 19th Century Lieder from female composers


(Optional) To regenerate metadata from upstream, 
clone <https://github.com/OpenScore/Lieder> (⚠️ wait for PR)
as a sibling of `dezrann-corpus`, then, from `Lieder`:
```sh
cd data
python to_dezrann.py
cp openscore-lieder-dezrann.json ../../dezrann-corpus/metadata/openscore-lieder.json`
```

Then, from `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build openscore-lieder
```

### weimar-jazz » Weimar Jazz Database

Follow instructions at <https://gitlab.com/algomus.fr/dezrann-corpus/corpus/weimar-jazz-database/jazz/README.md> to prepare all data.

Then, from `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build weimar-jazz
```

### supra » SUPRA

From `dezrann-corpus`, to prepare all data:

```sh
cd corpus/supra/src
yarn install
yarn start
```

(⚠️ TO BE CONFIRMED) Then, copy directly the data to your server:
```sh
scp -r pieces myhost:corpus/sandbox/supra
python tools/dezrann-corpus.py --build supra
```

### slovenian-folk-song-ballads » Slovenian Folk Song Ballads


From `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build slovenian-folk-song-ballads
```

Alternative option, after downloading long-term archive from  <https://doi.org/10.57745/SINZFK>:
```sh
python tools/dezrann-corpus.py --build --from-archive slovenian-folk-song-ballads.zip
```

### erkomaishvili » Traditional Georgian Sacred Music sung by Artem Erkomaishvili


(Optional) To regenerate metadata from upstream, from `dezrann-corpus`:
```sh
cd corpus/erkomaishvili/scripts
yarn install
yarn run corpus
python3 clean-scores.py
cp ../corpus/erkomaishvili-pieces.json ../../../metadata
```

Then, from `dezrann-corpus`:
```sh
python tools/dezrann-corpus.py --build erkomaishvili
```
